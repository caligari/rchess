//     rchess - a Rust library to manage chess structures
//     Copyright (C) 2022-2024  Rafa Couto <caligari@treboada.net>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::bitboard::*;
use super::pseudo_legals::*;
use crate::prelude::*;

lazy_static! {
    static ref EN_PASSANT_W: Vec<Bitboard> = (A3..=H3).map(precalc_en_passant_w).collect();
    static ref EN_PASSANT_B: Vec<Bitboard> = (A6..=H6).map(precalc_en_passant_b).collect();
}

// Six-Two Bitboards implementation
#[derive(Clone, Copy, Default)]
pub struct Board {
    white_pieces: Bitboard,
    black_pieces: Bitboard,
    p_pieces: Bitboard,
    n_pieces: Bitboard,
    b_pieces: Bitboard,
    r_pieces: Bitboard,
    q_pieces: Bitboard,
    k_pieces: Bitboard,
}

impl Board {
    pub fn from_initial() -> Self {
        Board {
            white_pieces: BITBOARD_INITIAL_WHITES,
            black_pieces: BITBOARD_INITIAL_BLACKS,
            p_pieces: BITBOARD_INITIAL_PAWNS,
            n_pieces: BITBOARD_INITIAL_KNIGHTS,
            b_pieces: BITBOARD_INITIAL_BISHOPS,
            r_pieces: BITBOARD_INITIAL_ROOKS,
            q_pieces: BITBOARD_INITIAL_QUEENS,
            k_pieces: BITBOARD_INITIAL_KINGS,
        }
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.white_pieces.is_empty() && self.black_pieces.is_empty()
    }

    #[inline]
    pub fn is_white_piece(&self, s: Square) -> bool {
        self.white_pieces.is_set(s)
    }

    #[inline]
    pub fn is_black_piece(&self, s: Square) -> bool {
        self.black_pieces.is_set(s)
    }

    #[inline]
    pub fn is_any_piece(&self, s: Square) -> bool {
        self.is_white_piece(s) || self.is_black_piece(s)
    }

    #[inline]
    pub fn is_pawn(&self, s: Square) -> bool {
        self.p_pieces.is_set(s)
    }

    #[inline]
    pub fn is_knight(&self, s: Square) -> bool {
        self.n_pieces.is_set(s)
    }

    #[inline]
    pub fn is_bishop(&self, s: Square) -> bool {
        self.b_pieces.is_set(s)
    }

    #[inline]
    pub fn is_rook(&self, s: Square) -> bool {
        self.r_pieces.is_set(s)
    }

    #[inline]
    pub fn is_queen(&self, s: Square) -> bool {
        self.q_pieces.is_set(s)
    }

    #[inline]
    pub fn is_king(&self, s: Square) -> bool {
        self.k_pieces.is_set(s)
    }

    pub fn get_color(&self, s: Square) -> Color {
        if self.is_white_piece(s) {
            COLOR_W
        } else if self.is_black_piece(s) {
            COLOR_B
        } else {
            COLOR_0
        }
    }

    pub fn get_piece_type(&self, s: Square) -> PieceType {
        if self.is_any_piece(s) {
            if self.is_pawn(s) {
                PIECE_P
            } else if self.is_knight(s) {
                PIECE_N
            } else if self.is_bishop(s) {
                PIECE_B
            } else if self.is_rook(s) {
                PIECE_R
            } else if self.is_queen(s) {
                PIECE_Q
            } else {
                PIECE_K
            }
        } else {
            PIECE_0
        }
    }

    pub fn get_piece(&self, s: Square) -> Piece {
        let piece_color = self.get_color(s);
        let piece_type = self.get_piece_type(s);
        (piece_type as i8) * piece_color
    }

    pub fn set_piece(&mut self, s: Square, p: Piece) {
        self.remove_piece(s); // clean previous piece
        if p != PIECE_00 {
            match p.piece() {
                PIECE_P => self.p_pieces.set(s),
                PIECE_N => self.n_pieces.set(s),
                PIECE_B => self.b_pieces.set(s),
                PIECE_R => self.r_pieces.set(s),
                PIECE_Q => self.q_pieces.set(s),
                PIECE_K => self.k_pieces.set(s),
                _ => unreachable!(),
            }
            match p.color() {
                COLOR_W => self.white_pieces.set(s),
                COLOR_B => self.black_pieces.set(s),
                _ => unreachable!(),
            }
        }
    }

    pub fn remove_piece(&mut self, s: Square) {
        self.white_pieces.clear(s);
        self.black_pieces.clear(s);
        self.p_pieces.clear(s);
        self.n_pieces.clear(s);
        self.b_pieces.clear(s);
        self.r_pieces.clear(s);
        self.q_pieces.clear(s);
        self.k_pieces.clear(s);
    }

    pub fn bitboard_occupancy(&self) -> Bitboard {
        self.white_pieces | self.black_pieces
    }

    pub fn bitboard_by_color(&self, color: Color) -> Bitboard {
        debug_assert_ne!(color, COLOR_0);
        *[&self.black_pieces, &self.white_pieces, &self.white_pieces][(color + 1) as usize]
    }

    pub fn bitboard_by_piece(&self, piece_type: PieceType) -> Bitboard {
        match piece_type {
            PIECE_K => self.k_pieces,
            PIECE_P => self.p_pieces,
            PIECE_N => self.n_pieces,
            PIECE_B => self.b_pieces,
            PIECE_R => self.r_pieces,
            PIECE_Q => self.q_pieces,
            _ => unreachable!(),
        }
    }

    pub fn squares_with_piece(&self, p: Piece) -> BitboardIter {
        debug_assert_ne!(p, PIECE_00);
        let bb_color = self.bitboard_by_color(p.color());
        let bb_type = self.bitboard_by_piece(p.piece());
        (bb_color & bb_type).iter()
    }

    /// Builds an 8-line string representing the board with piece chars
    pub fn lines(&self) -> String {
        self.lines_builder(crate::fen::piece_name_with_color)
    }

    pub fn lines_unicode(&self) -> String {
        self.lines_builder(piece_unicode)
    }

    pub fn attacked_squares(&self, attacker: Color) -> Bitboard {
        let mut attacks = Bitboard::default();
        self.bitboard_by_color(attacker)
            .iter()
            .for_each(|src| attacks |= pseudo_legals(*self, src));
        attacks
    }

    pub fn castling_free_wk_wq(&self) -> (bool, bool) {
        let attacks: u64 = self.attacked_squares(COLOR_B);
        let stoppers: u64 = self.bitboard_occupancy();
        (
            attacks & BITBOARD_CASTLING_WK_CHECK == 0 && stoppers & BITBOARD_CASTLING_WK_PATH == 0,
            attacks & BITBOARD_CASTLING_WQ_CHECK == 0 && stoppers & BITBOARD_CASTLING_WQ_PATH == 0,
        )
    }

    pub fn castling_free_bk_bq(&self) -> (bool, bool) {
        let attacks: u64 = self.attacked_squares(COLOR_W);
        let stoppers: u64 = self.bitboard_occupancy();
        (
            attacks & BITBOARD_CASTLING_BK_CHECK == 0 && stoppers & BITBOARD_CASTLING_BK_PATH == 0,
            attacks & BITBOARD_CASTLING_BQ_CHECK == 0 && stoppers & BITBOARD_CASTLING_BQ_PATH == 0,
        )
    }

    pub fn en_passant_captures_w(&self, square: Square) -> Bitboard {
        self.p_pieces & self.white_pieces & EN_PASSANT_B[square.file() as usize]
    }

    pub fn en_passant_captures_b(&self, square: Square) -> Bitboard {
        self.p_pieces & self.black_pieces & EN_PASSANT_W[square.file() as usize]
    }

    pub fn check_for_turn(&self, turn: Color) -> bool {
        let kings = match turn {
            COLOR_W => self.white_pieces & self.k_pieces,
            COLOR_B => self.black_pieces & self.k_pieces,
            _ => unreachable!(),
        };
        0 != self.attacked_squares(-turn) & kings
    }

    fn lines_builder<F: Fn(Piece) -> char>(&self, piece_name: F) -> String {
        (RANK_1..=RANK_8)
            .rev()
            .map(|r| {
                (FILE_A..=FILE_H)
                    .map(|f| piece_name(self.get_piece(Square::new(r, f))).to_string())
                    .collect::<Vec<String>>()
                    .join(" ")
            })
            .collect::<Vec<String>>()
            .join("\n")
    }
}

impl PartialEq for Board {
    fn eq(&self, other: &Self) -> bool {
        self.white_pieces == other.white_pieces
            && self.black_pieces == other.black_pieces
            && self.p_pieces == other.p_pieces
            && self.n_pieces == other.n_pieces
            && self.b_pieces == other.b_pieces
            && self.r_pieces == other.r_pieces
            && self.q_pieces == other.q_pieces
            && self.k_pieces == other.k_pieces
    }
}

fn precalc_en_passant_w(square: Square) -> Bitboard {
    let mut bb = Bitboard::default();
    let f = square.file();
    let r = square.rank();
    if f > FILE_A {
        bb.set(Square::new(r + 1, f - 1))
    }
    if f < FILE_H {
        bb.set(Square::new(r + 1, f + 1))
    }
    bb
}

fn precalc_en_passant_b(square: Square) -> Bitboard {
    let mut bb = Bitboard::default();
    let f = square.file();
    let r = square.rank();
    if f > FILE_A {
        bb.set(Square::new(r - 1, f - 1))
    }
    if f < FILE_H {
        bb.set(Square::new(r - 1, f + 1))
    }
    bb
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_board_ranks() {
        // 64 squares in 8 ranks
        for r in RANK_1..=RANK_8 {
            let rank = RANKS[r as usize];
            assert!(rank.len() == 8);

            // square in rank
            for s in rank.iter() {
                assert!(s.rank() == r);
            }
        }
    }

    #[test]
    fn test_board_files() {
        // 64 squares in 8 files
        for f in FILE_A..=FILE_H {
            let file = FILES[f as usize];
            assert!(file.len() == 8);

            // square in file
            for s in file.iter() {
                assert!(s.file() == f);
            }
        }
    }

    #[test]
    fn test_board_empty() {
        // board
        let b = Board::default();
        assert!(b.is_empty());

        // squares
        for square in A1..=H8 {
            assert!(!b.is_any_piece(square));
            assert!(!b.is_white_piece(square));
            assert!(!b.is_black_piece(square));
            assert!(b.get_piece_type(square) == PIECE_0);
        }
    }

    #[test]
    fn test_board_initial() {
        // empty squares
        let b = Board::from_initial();
        for r in RANK_3..=RANK_6 {
            for square in RANKS[r as usize] {
                assert!(!b.is_any_piece(*square));
                assert!(!b.is_white_piece(*square));
                assert!(!b.is_black_piece(*square));
                assert!(b.get_piece_type(*square) == PIECE_0);
            }
        }

        // white pieces
        for r in [RANK_1, RANK_2].into_iter() {
            for square in RANKS[r as usize] {
                assert!(b.is_white_piece(*square));
                assert!(!b.is_black_piece(*square));
                assert!(b.get_piece_type(*square) != PIECE_0);
            }
        }

        // black pieces
        for r in [RANK_7, RANK_8].into_iter() {
            for square in RANKS[r as usize] {
                assert!(b.is_black_piece(*square));
                assert!(!b.is_white_piece(*square));
                assert!(b.get_piece_type(*square) != PIECE_0);
            }
        }

        // pawns
        for r in [RANK_2, RANK_7].into_iter() {
            for square in RANKS[r as usize] {
                assert!(b.is_pawn(*square));
            }
        }

        // knights
        assert!(b.is_knight(B1));
        assert!(b.is_knight(G1));
        assert!(b.is_knight(B8));
        assert!(b.is_knight(G8));

        // bishops
        assert!(b.is_bishop(C1));
        assert!(b.is_bishop(F1));
        assert!(b.is_bishop(C8));
        assert!(b.is_bishop(F8));

        // rooks
        assert!(b.is_rook(A1));
        assert!(b.is_rook(H1));
        assert!(b.is_rook(A8));
        assert!(b.is_rook(H8));

        // queens
        assert!(b.is_queen(D1));
        assert!(b.is_queen(D8));

        // kings
        assert!(b.is_king(E1));
        assert!(b.is_king(E8));
    }

    #[test]
    fn test_board_move() {
        let mut b = Board::from_initial();

        // e2-e4
        let piece = b.get_piece(E2);
        b.remove_piece(E2);
        b.set_piece(E4, piece);

        assert!(!b.is_any_piece(E2));
        assert!(b.is_any_piece(E4));
        assert!(b.is_white_piece(E4));
        assert!(b.is_pawn(E4));
    }

    #[test]
    fn test_en_passant_capturers() {
        let board = crate::fen::parse_fen_board("8/8/8/pPpPppPp/PpPPpPpP/8/8/8").unwrap();
        // rank 6 capturers
        let capturers = board.en_passant_captures_w(A6);
        assert_eq!(1, capturers.active_bits_count());
        assert!(capturers.is_set(B5));
        let capturers = board.en_passant_captures_w(C6);
        assert_eq!(2, capturers.active_bits_count());
        assert!(capturers.is_set(B5));
        assert!(capturers.is_set(D5));
        let capturers = board.en_passant_captures_w(H6);
        assert_eq!(1, capturers.active_bits_count());
        assert!(capturers.is_set(G5));
        // rank 3 capturers
        let capturers = board.en_passant_captures_b(A3);
        assert_eq!(1, capturers.active_bits_count());
        assert!(capturers.is_set(B4));
        let capturers = board.en_passant_captures_b(F3);
        assert_eq!(2, capturers.active_bits_count());
        assert!(capturers.is_set(E4));
        assert!(capturers.is_set(G4));
        let capturers = board.en_passant_captures_b(H3);
        assert_eq!(1, capturers.active_bits_count());
        assert!(capturers.is_set(G4));
    }

    #[test]
    fn test_lines() {
        let lines = Board::from_initial().lines();
        // 8 rows separated by new line
        let rows: Vec<_> = lines.split("\n").collect();
        assert_eq!(8, rows.len());
        // board representation
        assert_eq!(rows[0], "r n b q k b n r");
        assert_eq!(rows[1], "p p p p p p p p");
        assert_eq!(rows[2], ". . . . . . . .");
        assert_eq!(rows[3], ". . . . . . . .");
        assert_eq!(rows[4], ". . . . . . . .");
        assert_eq!(rows[5], ". . . . . . . .");
        assert_eq!(rows[6], "P P P P P P P P");
        assert_eq!(rows[7], "R N B Q K B N R");
    }

    #[test]
    fn test_lines_unicode() {
        let lines = Board::from_initial().lines_unicode();
        // 8 rows separated by new line
        let rows: Vec<_> = lines.split("\n").collect();
        assert_eq!(8, rows.len());
        // board representation
        assert_eq!(rows[0], "♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜");
        assert_eq!(rows[1], "♟ ♟ ♟ ♟ ♟ ♟ ♟ ♟");
        assert_eq!(rows[2], "               ");
        assert_eq!(rows[3], "               ");
        assert_eq!(rows[4], "               ");
        assert_eq!(rows[5], "               ");
        assert_eq!(rows[6], "♙ ♙ ♙ ♙ ♙ ♙ ♙ ♙");
        assert_eq!(rows[7], "♖ ♘ ♗ ♕ ♔ ♗ ♘ ♖");
    }
}

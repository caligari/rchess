//     rchess - a Rust library to manage chess structures
//     Copyright (C) 2022-2024  Rafa Couto <caligari@treboada.net>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program. If not, see <https://www.gnu.org/licenses/>.

pub(crate) type PieceType = u8;
pub const PIECE_0: PieceType = 0;
pub const PIECE_P: PieceType = 1;
pub const PIECE_N: PieceType = 2;
pub const PIECE_B: PieceType = 3;
pub const PIECE_R: PieceType = 4;
pub const PIECE_Q: PieceType = 5;
pub const PIECE_K: PieceType = 6;

pub(crate) type Piece = i8;
pub const PIECE_00: Piece = PIECE_0 as Piece;
pub const PIECE_WP: Piece = PIECE_P as Piece;
pub const PIECE_WN: Piece = PIECE_N as Piece;
pub const PIECE_WB: Piece = PIECE_B as Piece;
pub const PIECE_WR: Piece = PIECE_R as Piece;
pub const PIECE_WQ: Piece = PIECE_Q as Piece;
pub const PIECE_WK: Piece = PIECE_K as Piece;
pub const PIECE_BP: Piece = -(PIECE_P as Piece);
pub const PIECE_BN: Piece = -(PIECE_N as Piece);
pub const PIECE_BB: Piece = -(PIECE_B as Piece);
pub const PIECE_BR: Piece = -(PIECE_R as Piece);
pub const PIECE_BQ: Piece = -(PIECE_Q as Piece);
pub const PIECE_BK: Piece = -(PIECE_K as Piece);

pub(crate) type Color = i8;
pub const COLOR_0: i8 = 0;
pub const COLOR_W: i8 = 1;
pub const COLOR_B: i8 = -1;

pub trait PieceFeatures {
    fn new(p: PieceType, c: Color) -> Self;
    fn color(&self) -> Color;
    fn piece(&self) -> PieceType;
}

impl PieceFeatures for Piece {
    #[inline]
    fn new(p: PieceType, c: Color) -> Self {
        (p as i8) * c
    }

    #[inline]
    fn color(&self) -> Color {
        self.signum()
    }

    #[inline]
    fn piece(&self) -> PieceType {
        self.unsigned_abs()
    }
}

const PIECE_NAMES: [char; 7] = ['.', 'P', 'N', 'B', 'R', 'Q', 'K'];

#[inline]
pub fn piece_name(p: PieceType) -> char {
    PIECE_NAMES[p as usize]
}

pub fn parse_piece(c: char) -> Option<Piece> {
    let color = match c.is_ascii_uppercase() {
        true => COLOR_W,
        false => COLOR_B,
    };
    PIECE_NAMES
        .iter()
        .position(|&p| p == c.to_ascii_uppercase())
        .map(|p| p as i8 * color)
}

const WHITE_UNICODE: [char; 7] = [
    ' ',        // whitespace
    '\u{2659}', // white pawn
    '\u{2658}', // white knight
    '\u{2657}', // white bishop
    '\u{2656}', // white rook
    '\u{2655}', // white queen
    '\u{2654}', // white king
];
const BLACK_UNICODE: [char; 7] = [
    ' ',        // whitespace
    '\u{265f}', // white pawn
    '\u{265e}', // white knight
    '\u{265d}', // white bishop
    '\u{265c}', // white rook
    '\u{265b}', // white queen
    '\u{265a}', // white king
];

pub fn piece_unicode(p: Piece) -> char {
    match p.color() {
        COLOR_W => WHITE_UNICODE[p.piece() as usize],
        COLOR_B => BLACK_UNICODE[p.piece() as usize],
        _ => ' ',
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_unicode_chars() {
        assert_eq!('♙', piece_unicode(PIECE_WP));
        assert_eq!('♘', piece_unicode(PIECE_WN));
        assert_eq!('♗', piece_unicode(PIECE_WB));
        assert_eq!('♖', piece_unicode(PIECE_WR));
        assert_eq!('♕', piece_unicode(PIECE_WQ));
        assert_eq!('♔', piece_unicode(PIECE_WK));
        assert_eq!('♟', piece_unicode(PIECE_BP));
        assert_eq!('♞', piece_unicode(PIECE_BN));
        assert_eq!('♝', piece_unicode(PIECE_BB));
        assert_eq!('♜', piece_unicode(PIECE_BR));
        assert_eq!('♛', piece_unicode(PIECE_BQ));
        assert_eq!('♚', piece_unicode(PIECE_BK));
    }
}

//     rchess - a Rust library to manage chess structures
//     Copyright (C) 2022-2024  Rafa Couto <caligari@treboada.net>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::bitboard::*;
use crate::magic_bitboards;
use crate::prelude::*;

/// Calculates the pseudo-legal moves for a piece in a square of the board. Pseudo-legal moves
/// are legal in the sense that they are consistent with the current board representation.
///
/// This implementation uses precalculated look-up tables for a fast move generation, including
/// the [magic bitboards][magic_bitboards] for the slicing pieces (rook, bishop and queen).
///
/// The possible moves are represented by the destination squares within a bit-board.
///
/// ```
/// use rchess::prelude::*;
/// use rchess::pseudo_legals::*;
///
/// let board = Board::from_initial();
/// let moves = pseudo_legals(board, G1);
/// assert_eq!(moves.lines(), "\
///     . . . . . . . .\n\
///     . . . . . . . .\n\
///     . . . . . . . .\n\
///     . . . . . . . .\n\
///     . . . . . . . .\n\
///     . . . . . 1 . 1\n\
///     . . . . . . . .\n\
///     . . . . . . . .");
/// ```
///
/// This example calculates the pseudo-legal moves for a Knight on G1 of the initial position.
/// Active bits are the F3 and H3, the destination squares for the Knight moves.
///
/// [magic_bitboards]: https://www.chessprogramming.org/Magic_Bitboards
///
pub fn pseudo_legals(board: Board, square: Square) -> Bitboard {
    let piece = board.get_piece_type(square) as usize;
    PSEUDO_LEGALS_F[piece](board, square)
}

lazy_static! {
    static ref ATTACKS_N: Vec<Bitboard> = (A1..=H8).map(precalc_attacks_n).collect();
    static ref ATTACKS_K: Vec<Bitboard> = (A1..=H8).map(precalc_attacks_k).collect();
    static ref ATTACKS_WP: Vec<Bitboard> = (A1..=H8).map(precalc_attacks_wp).collect();
    static ref ATTACKS_BP: Vec<Bitboard> = (A1..=H8).map(precalc_attacks_bp).collect();
}

const PSEUDO_LEGALS_F: [fn(b: Board, s: Square) -> Bitboard; 7] = [
    pseudo_legals_0, // const PIECE_0: u8 = 0;
    pseudo_legals_p, // const PIECE_P: u8 = 1;
    pseudo_legals_n, // const PIECE_N: u8 = 2;
    pseudo_legals_b, // const PIECE_B: u8 = 3;
    pseudo_legals_r, // const PIECE_R: u8 = 4;
    pseudo_legals_q, // const PIECE_Q: u8 = 5;
    pseudo_legals_k, // const PIECE_K: u8 = 6;
];

fn pseudo_legals_0(_board: Board, _square: Square) -> Bitboard {
    Bitboard::default()
}

fn pseudo_legals_n(board: Board, square: Square) -> Bitboard {
    ATTACKS_N[square as usize] & !(board.bitboard_by_color(board.get_color(square)))
}

fn pseudo_legals_k(board: Board, square: Square) -> Bitboard {
    ATTACKS_K[square as usize] & !(board.bitboard_by_color(board.get_color(square)))
}

fn pseudo_legals_b(board: Board, square: Square) -> Bitboard {
    magic_bitboards::get_attacks_b(board.bitboard_occupancy(), square)
        & !(board.bitboard_by_color(board.get_color(square)))
}

fn pseudo_legals_r(board: Board, square: Square) -> Bitboard {
    magic_bitboards::get_attacks_r(board.bitboard_occupancy(), square)
        & !(board.bitboard_by_color(board.get_color(square)))
}

fn pseudo_legals_q(board: Board, square: Square) -> Bitboard {
    (magic_bitboards::get_attacks_b(board.bitboard_occupancy(), square)
        | magic_bitboards::get_attacks_r(board.bitboard_occupancy(), square))
        & !(board.bitboard_by_color(board.get_color(square)))
}

fn pseudo_legals_p(board: Board, square: Square) -> Bitboard {
    let color = board.get_color(square);
    let rank = square.rank();
    match color {
        COLOR_W => {
            let mut movements = ATTACKS_WP[square as usize] & board.bitboard_by_color(COLOR_B);
            if rank < RANK_8 {
                let mut dst = square + 8;
                if !board.is_any_piece(dst) {
                    movements.set(dst);
                    if rank == RANK_2 {
                        dst += 8;
                        if !board.is_any_piece(dst) {
                            movements.set(dst);
                        }
                    }
                }
            }
            movements
        }
        COLOR_B => {
            let mut movements = ATTACKS_BP[square as usize] & board.bitboard_by_color(COLOR_W);
            if rank > RANK_1 {
                let mut dst = square - 8;
                if !board.is_any_piece(dst) {
                    movements.set(dst);
                    if rank == RANK_7 {
                        dst -= 8;
                        if !board.is_any_piece(dst) {
                            movements.set(dst);
                        }
                    }
                }
            }
            movements
        }
        _ => unreachable!(),
    }
}

fn precalc_attacks_wp(origin: Square) -> Bitboard {
    let mut bb = Bitboard::default();
    let rank_src = origin.rank();
    let file_src = origin.file();
    if rank_src < RANK_8 {
        let rank_dst = rank_src + 1;
        if file_src < FILE_H {
            bb.set(Square::new(rank_dst, file_src + 1));
        }
        if file_src > FILE_A {
            bb.set(Square::new(rank_dst, file_src - 1));
        }
    }
    bb
}

fn precalc_attacks_bp(origin: Square) -> Bitboard {
    let mut bb = Bitboard::default();
    let rank_src = origin.rank();
    let file_src = origin.file();
    if rank_src > RANK_1 {
        let rank_dst = rank_src - 1;
        if file_src < FILE_H {
            bb.set(Square::new(rank_dst, file_src + 1));
        }
        if file_src > FILE_A {
            bb.set(Square::new(rank_dst, file_src - 1));
        }
    }
    bb
}

fn precalc_attacks_n(origin: Square) -> Bitboard {
    let mut bb = Bitboard::default();
    let r = origin.rank();
    let f = origin.file();
    if r <= RANK_6 {
        if f > FILE_A {
            bb.set(Square::new(r + 2, f - 1));
        }
        if f < FILE_H {
            bb.set(Square::new(r + 2, f + 1));
        }
    }
    if r >= RANK_3 {
        if f > FILE_A {
            bb.set(Square::new(r - 2, f - 1));
        }
        if f < FILE_H {
            bb.set(Square::new(r - 2, f + 1));
        }
    }
    if f >= FILE_C {
        if r < RANK_8 {
            bb.set(Square::new(r + 1, f - 2));
        }
        if r > RANK_1 {
            bb.set(Square::new(r - 1, f - 2));
        }
    }
    if f <= FILE_F {
        if r < RANK_8 {
            bb.set(Square::new(r + 1, f + 2));
        }
        if r > RANK_1 {
            bb.set(Square::new(r - 1, f + 2));
        }
    }
    bb
}

fn precalc_attacks_k(origin: Square) -> Bitboard {
    let mut bb = Bitboard::default();
    let r = origin.rank();
    let f = origin.file();
    if r < RANK_8 && f < FILE_H {
        bb.set(Square::new(r + 1, f + 1));
    }
    if r > RANK_1 && f < FILE_H {
        bb.set(Square::new(r - 1, f + 1));
    }
    if r > RANK_1 && f > FILE_A {
        bb.set(Square::new(r - 1, f - 1));
    }
    if r < RANK_8 && f > FILE_A {
        bb.set(Square::new(r + 1, f - 1));
    }
    if r < RANK_8 {
        bb.set(Square::new(r + 1, f));
    }
    if r > RANK_1 {
        bb.set(Square::new(r - 1, f));
    }
    if f < FILE_H {
        bb.set(Square::new(r, f + 1));
    }
    if f > FILE_A {
        bb.set(Square::new(r, f - 1));
    }
    bb
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pseudo_legals_n() {
        let mut b = Board::default();
        b.set_piece(D4, PIECE_WN);
        b.set_piece(C2, PIECE_BN);
        b.set_piece(A1, PIECE_BN);

        let movements = pseudo_legals(b, D4);
        assert_eq!(0x0000142200221400_u64, movements.into());

        let movements = pseudo_legals(b, C2);
        assert_eq!(0x000000000a110010_u64, movements.into());

        let movements = pseudo_legals(b, A1);
        assert_eq!(0x0000000000020000_u64, movements.into());
    }

    #[test]
    fn test_pseudo_legals_k() {
        let mut b = Board::default();

        b.set_piece(E1, PIECE_WK);
        let movements = pseudo_legals(b, E1);
        assert_eq!(0x0000000000003828_u64, movements.into());

        b.set_piece(E7, PIECE_BK);
        b.set_piece(D7, PIECE_BP);
        b.set_piece(E6, PIECE_WP);
        let movements = pseudo_legals(b, E7);
        assert_eq!(0x3820380000000000_u64, movements.into());
    }

    #[test]
    fn test_pseudo_legals_p() {
        let mut b = Board::default();

        // advance 2 from initial
        b.set_piece(E2, PIECE_WP);
        b.set_piece(E7, PIECE_BP);
        let movements = pseudo_legals(b, E2);
        assert_eq!(0x0000000010100000_u64, movements.into());
        let movements = pseudo_legals(b, E7);
        assert_eq!(0x0000101000000000_u64, movements.into());

        // blocked advance 2
        b.set_piece(E4, PIECE_BN);
        b.set_piece(E5, PIECE_WN);
        let movements = pseudo_legals(b, E2);
        assert_eq!(0x0000000000100000_u64, movements.into());
        let movements = pseudo_legals(b, E7);
        assert_eq!(0x0000100000000000_u64, movements.into());

        // fully blocked
        b.remove_piece(E4);
        b.remove_piece(E5);
        b.set_piece(E3, PIECE_BN);
        b.set_piece(E6, PIECE_WN);
        let movements = pseudo_legals(b, E2);
        assert_eq!(0x0000000000000000_u64, movements.into());
        let movements = pseudo_legals(b, E7);
        assert_eq!(0x0000000000000000_u64, movements.into());

        // captures
        b.set_piece(D3, PIECE_BN);
        b.set_piece(F3, PIECE_BN);
        b.set_piece(D6, PIECE_WN);
        b.set_piece(F6, PIECE_WN);
        let movements = pseudo_legals(b, E2);
        assert_eq!(0x0000000000280000_u64, movements.into());
        let movements = pseudo_legals(b, E7);
        assert_eq!(0x0000280000000000_u64, movements.into());

        // no captures
        b.remove_piece(D3);
        b.remove_piece(F3);
        b.remove_piece(D6);
        b.remove_piece(F6);
        b.set_piece(D3, PIECE_WP);
        b.set_piece(F3, PIECE_WP);
        b.set_piece(D6, PIECE_BP);
        b.set_piece(F6, PIECE_BP);
        let movements = pseudo_legals(b, E2);
        assert_eq!(0x0000000000000000_u64, movements.into());
        let movements = pseudo_legals(b, E7);
        assert_eq!(0x0000000000000000_u64, movements.into());
    }

    #[test]
    fn test_pseudo_legals_r() {
        let mut b = Board::from_initial();
        let movements = pseudo_legals(b, A1);
        assert_eq!(0x0000000000000000_u64, movements.into());

        b.remove_piece(A2);
        let movements = pseudo_legals(b, A1);
        assert_eq!(0x0001010101010100, u64::from(movements));

        b.remove_piece(B1);
        let movements = pseudo_legals(b, A1);
        assert_eq!(0x0001010101010102, u64::from(movements));

        b.set_piece(A5, PIECE_BP);
        let movements = pseudo_legals(b, A1);
        assert_eq!(0x0000000101010102, u64::from(movements));

        b.set_piece(A3, PIECE_WN);
        let movements = pseudo_legals(b, A1);
        assert_eq!(0x0000000000000102, u64::from(movements));

        b.set_piece(B5, PIECE_WR);
        let movements = pseudo_legals(b, B5);
        assert_eq!(0x000202fd02020000, u64::from(movements));

        b.remove_piece(B5);
        b.set_piece(B5, PIECE_BR);
        let movements = pseudo_legals(b, B5);
        assert_eq!(0x000002fc02020200, u64::from(movements));
    }

    #[test]
    fn test_pseudo_legals_b() {
        let mut b = Board::from_initial();
        let movements = pseudo_legals(b, C1);
        assert_eq!(0x0000000000000000, u64::from(movements));

        b.remove_piece(D2);
        let movements = pseudo_legals(b, C1);
        assert_eq!(0x0000804020100800, u64::from(movements));

        b.set_piece(G5, PIECE_BP);
        let movements = pseudo_legals(b, C1);
        assert_eq!(0x0000004020100800, u64::from(movements));

        b.set_piece(F4, PIECE_WP);
        b.remove_piece(B2);
        let movements = pseudo_legals(b, C1);
        assert_eq!(0x0000000000110a00, u64::from(movements));

        b.set_piece(D4, PIECE_WB);
        let movements = pseudo_legals(b, D4);
        assert_eq!(0x0041221400140200, u64::from(movements));
    }

    #[test]
    fn test_pseudo_legals_q() {
        let mut b = Board::default();
        b.set_piece(E5, PIECE_WQ);

        let movements = pseudo_legals(b, E5);
        assert_eq!(0x925438ef38549211, u64::from(movements));

        let mut b = Board::from_initial();
        b.set_piece(E5, PIECE_WQ);
        let movements = pseudo_legals(b, E5);
        assert_eq!(0x005438ef38540000, u64::from(movements));
    }
}

//     rchess - a Rust library to manage chess structures
//     Copyright (C) 2022-2024  Rafa Couto <caligari@treboada.net>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program. If not, see <https://www.gnu.org/licenses/>.

pub(crate) type Rank = u8;
pub const RANK_1: Rank = 0;
pub const RANK_2: Rank = 1;
pub const RANK_3: Rank = 2;
pub const RANK_4: Rank = 3;
pub const RANK_5: Rank = 4;
pub const RANK_6: Rank = 5;
pub const RANK_7: Rank = 6;
pub const RANK_8: Rank = 7;

pub(crate) type File = u8;
pub const FILE_A: File = 0;
pub const FILE_B: File = 1;
pub const FILE_C: File = 2;
pub const FILE_D: File = 3;
pub const FILE_E: File = 4;
pub const FILE_F: File = 5;
pub const FILE_G: File = 6;
pub const FILE_H: File = 7;

pub(crate) type Square = u8;
pub const A1: Square = 0;
pub const B1: Square = 1;
pub const C1: Square = 2;
pub const D1: Square = 3;
pub const E1: Square = 4;
pub const F1: Square = 5;
pub const G1: Square = 6;
pub const H1: Square = 7;
pub const A2: Square = 8;
pub const B2: Square = 9;
pub const C2: Square = 10;
pub const D2: Square = 11;
pub const E2: Square = 12;
pub const F2: Square = 13;
pub const G2: Square = 14;
pub const H2: Square = 15;
pub const A3: Square = 16;
pub const B3: Square = 17;
pub const C3: Square = 18;
pub const D3: Square = 19;
pub const E3: Square = 20;
pub const F3: Square = 21;
pub const G3: Square = 22;
pub const H3: Square = 23;
pub const A4: Square = 24;
pub const B4: Square = 25;
pub const C4: Square = 26;
pub const D4: Square = 27;
pub const E4: Square = 28;
pub const F4: Square = 29;
pub const G4: Square = 30;
pub const H4: Square = 31;
pub const A5: Square = 32;
pub const B5: Square = 33;
pub const C5: Square = 34;
pub const D5: Square = 35;
pub const E5: Square = 36;
pub const F5: Square = 37;
pub const G5: Square = 38;
pub const H5: Square = 39;
pub const A6: Square = 40;
pub const B6: Square = 41;
pub const C6: Square = 42;
pub const D6: Square = 43;
pub const E6: Square = 44;
pub const F6: Square = 45;
pub const G6: Square = 46;
pub const H6: Square = 47;
pub const A7: Square = 48;
pub const B7: Square = 49;
pub const C7: Square = 50;
pub const D7: Square = 51;
pub const E7: Square = 52;
pub const F7: Square = 53;
pub const G7: Square = 54;
pub const H7: Square = 55;
pub const A8: Square = 56;
pub const B8: Square = 57;
pub const C8: Square = 58;
pub const D8: Square = 59;
pub const E8: Square = 60;
pub const F8: Square = 61;
pub const G8: Square = 62;
pub const H8: Square = 63;

pub const BOARD_SQUARES: [Square; 64] = [
    A1, B1, C1, D1, E1, F1, G1, H1, // rank 1
    A2, B2, C2, D2, E2, F2, G2, H2, // rank 2
    A3, B3, C3, D3, E3, F3, G3, H3, // rank 3
    A4, B4, C4, D4, E4, F4, G4, H4, // rank 4
    A5, B5, C5, D5, E5, F5, G5, H5, // rank 5
    A6, B6, C6, D6, E6, F6, G6, H6, // rank 6
    A7, B7, C7, D7, E7, F7, G7, H7, // rank 7
    A8, B8, C8, D8, E8, F8, G8, H8, // rank 8
];

pub const RANK_1_SQUARES: [Square; 8] = [A1, B1, C1, D1, E1, F1, G1, H1];
pub const RANK_2_SQUARES: [Square; 8] = [A2, B2, C2, D2, E2, F2, G2, H2];
pub const RANK_3_SQUARES: [Square; 8] = [A3, B3, C3, D3, E3, F3, G3, H3];
pub const RANK_4_SQUARES: [Square; 8] = [A4, B4, C4, D4, E4, F4, G4, H4];
pub const RANK_5_SQUARES: [Square; 8] = [A5, B5, C5, D5, E5, F5, G5, H5];
pub const RANK_6_SQUARES: [Square; 8] = [A6, B6, C6, D6, E6, F6, G6, H6];
pub const RANK_7_SQUARES: [Square; 8] = [A7, B7, C7, D7, E7, F7, G7, H7];
pub const RANK_8_SQUARES: [Square; 8] = [A8, B8, C8, D8, E8, F8, G8, H8];

pub const FILE_1_SQUARES: [Square; 8] = [A1, A2, A3, A4, A5, A6, A7, A8];
pub const FILE_2_SQUARES: [Square; 8] = [B1, B2, B3, B4, B5, B6, B7, B8];
pub const FILE_3_SQUARES: [Square; 8] = [C1, C2, C3, C4, C5, C6, C7, C8];
pub const FILE_4_SQUARES: [Square; 8] = [D1, D2, D3, D4, D5, D6, D7, D8];
pub const FILE_5_SQUARES: [Square; 8] = [E1, E2, E3, E4, E5, E6, E7, E8];
pub const FILE_6_SQUARES: [Square; 8] = [F1, F2, F3, F4, F5, F6, F7, F8];
pub const FILE_7_SQUARES: [Square; 8] = [G1, G2, G3, G4, G5, G6, G7, G8];
pub const FILE_8_SQUARES: [Square; 8] = [H1, H2, H3, H4, H5, H6, H7, H8];

pub const RANKS: [&[Square; 8]; 8] = [
    &RANK_1_SQUARES,
    &RANK_2_SQUARES,
    &RANK_3_SQUARES,
    &RANK_4_SQUARES,
    &RANK_5_SQUARES,
    &RANK_6_SQUARES,
    &RANK_7_SQUARES,
    &RANK_8_SQUARES,
];

pub const FILES: [&[Square; 8]; 8] = [
    &FILE_1_SQUARES,
    &FILE_2_SQUARES,
    &FILE_3_SQUARES,
    &FILE_4_SQUARES,
    &FILE_5_SQUARES,
    &FILE_6_SQUARES,
    &FILE_7_SQUARES,
    &FILE_8_SQUARES,
];

pub trait SquareCoordinates {
    fn rank(&self) -> Rank;
    fn file(&self) -> File;
    fn new(r: Rank, f: File) -> Self;
}

impl SquareCoordinates for Square {
    #[inline]
    fn rank(&self) -> Rank {
        self / 8
    }

    #[inline]
    fn file(&self) -> File {
        self % 8
    }

    #[inline]
    fn new(r: Rank, f: File) -> Self {
        (r * 8) + f
    }
}

const SQUARE_NAMES: [&str; 64] = [
    "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1", // rank 1
    "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2", // rank 2
    "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3", // rank 3
    "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4", // rank 4
    "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5", // rank 5
    "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6", // rank 6
    "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7", // rank 7
    "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8", // rank 8
];

#[inline]
pub fn square_name(s: u8) -> &'static str {
    SQUARE_NAMES[s as usize]
}

pub fn parse_square(s: &str) -> Option<Square> {
    let square_name = s.to_ascii_lowercase();
    let pos = SQUARE_NAMES.iter().position(|sn| &square_name == sn);
    pos.map(|p| p as Square)
}

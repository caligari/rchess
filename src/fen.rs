//     rchess - a Rust library to manage chess structures
//     Copyright (C) 2022-2024  Rafa Couto <caligari@treboada.net>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::prelude::*;

pub fn piece_name_with_color(p: Piece) -> char {
    if p.color() == COLOR_W {
        piece_name(p.piece())
    } else {
        piece_name(p.piece()).to_ascii_lowercase()
    }
}

fn fen_board_rank(b: &Board, r: Rank) -> String {
    let mut fen = String::new();
    let mut empties = 0;
    for s in RANKS[r as usize].iter() {
        if b.is_any_piece(*s) {
            if empties != 0 {
                fen.push_str(empties.to_string().as_str());
                empties = 0;
            }
            let piece = b.get_piece(*s);
            fen.push(piece_name_with_color(piece));
        } else {
            empties += 1;
        }
    }
    if empties != 0 {
        fen.push_str(empties.to_string().as_str());
    }
    fen
}

pub fn fen_board(b: &Board) -> String {
    let mut ranks: Vec<String> = Vec::new();

    (RANK_1..=RANK_8).rev().for_each(|r| {
        ranks.push(fen_board_rank(b, r));
    });

    ranks.join("/")
}

fn fen_castling(p: &Position) -> String {
    let mut fen = String::new();
    if p.is_castling_able(Castling::WK) {
        fen.push('K');
    }
    if p.is_castling_able(Castling::WQ) {
        fen.push('Q');
    }
    if p.is_castling_able(Castling::BK) {
        fen.push('k');
    }
    if p.is_castling_able(Castling::BQ) {
        fen.push('q');
    }
    if fen.is_empty() {
        fen.push('-');
    }
    fen
}

fn fen_en_passant(p: &Position) -> &str {
    match p.get_en_passant() {
        Some(square) => square_name(square),
        _ => "-",
    }
}

pub fn fen_position(p: &Position) -> String {
    let mut fen = fen_board(p.get_board());
    fen.push(' ');
    fen.push(match p.get_side_to_move() {
        COLOR_W => 'w',
        COLOR_B => 'b',
        _ => '?',
    });
    fen.push(' ');
    fen.push_str(fen_castling(p).as_str());
    fen.push(' ');
    fen.push_str(fen_en_passant(p));
    fen.push(' ');
    fen.push_str(p.get_half_move_clock().to_string().as_str());
    fen
}

#[non_exhaustive]
#[derive(Debug)]
pub enum ParseFenError {
    MissingField,
    OutOfBounds,
    UnexpectedChar,
}

pub fn parse_fen_board(fen: &str) -> Result<Board, ParseFenError> {
    let mut board = Board::default();
    let mut rank = RANK_1;
    for rank_str in fen.split('/') {
        if rank > RANK_8 {
            return Err(ParseFenError::OutOfBounds);
        }
        let mut file = FILE_A;
        for c in rank_str.chars() {
            if file > FILE_H {
                return Err(ParseFenError::OutOfBounds);
            }
            let piece = parse_piece(c);
            if let Some(p) = piece {
                board.set_piece(Square::new(7 - rank, file), p);
                file += 1;
            } else {
                let empties = c.to_string().parse::<u8>();
                if empties.is_err() {
                    return Err(ParseFenError::UnexpectedChar);
                }
                let mut skip = empties.unwrap();
                if !(1..=8).contains(&skip) {
                    return Err(ParseFenError::OutOfBounds);
                }
                while skip > 0 {
                    if file > FILE_H {
                        return Err(ParseFenError::OutOfBounds);
                    }
                    file += 1;
                    skip -= 1;
                }
            }
        }
        if file != 8 {
            return Err(ParseFenError::OutOfBounds);
        }
        rank += 1;
    }
    if rank != 8 {
        return Err(ParseFenError::OutOfBounds);
    }
    Ok(board)
}

fn parse_fen_castling(fen: &str) -> Result<u8, ParseFenError> {
    let mut castling = Castling::None as u8;
    if fen != "-" {
        for c in fen.chars() {
            match c {
                'K' => castling |= Castling::WK as u8,
                'Q' => castling |= Castling::WQ as u8,
                'k' => castling |= Castling::BK as u8,
                'q' => castling |= Castling::BQ as u8,
                _ => return Err(ParseFenError::UnexpectedChar),
            };
        }
    }
    Ok(castling)
}

pub fn parse_fen_position(fen: &str) -> Result<Position, ParseFenError> {
    let mut fields = fen.split(' ');

    let field = fields.next();
    if field.is_none() {
        return Err(ParseFenError::MissingField);
    }
    let board = parse_fen_board(field.unwrap())?;

    let turn: Color = match fields.next() {
        Some("w") => COLOR_W,
        Some("b") => COLOR_B,
        _ => return Err(ParseFenError::UnexpectedChar),
    };

    let field = fields.next();
    if field.is_none() {
        return Err(ParseFenError::MissingField);
    }
    let castling = parse_fen_castling(field.unwrap())?;

    let field = fields.next();
    if field.is_none() {
        return Err(ParseFenError::MissingField);
    }
    let en_passant = parse_square(field.unwrap());

    let field = fields.next();
    if field.is_none() {
        return Err(ParseFenError::MissingField);
    }
    let hm_clock = field.unwrap().parse::<u8>();
    if hm_clock.is_err() {
        return Err(ParseFenError::UnexpectedChar);
    }

    Ok(Position::with_params(
        &board,
        turn,
        castling,
        en_passant,
        hm_clock.unwrap(),
    ))
}

#[cfg(test)]
mod tests {
    use crate::bitboard::BitboardTrait;

    use super::*;

    const FEN_BOARD_EMPTY: &'static str = "8/8/8/8/8/8/8/8";
    const FEN_BOARD_INITIAL: &'static str = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR";

    #[test]
    fn test_fen_board() {
        let b = Board::default();
        assert!(fen_board(&b) == FEN_BOARD_EMPTY);

        let b = Board::from_initial();
        assert!(fen_board(&b) == FEN_BOARD_INITIAL);

        let mut b = Board::default();
        b.set_piece(B1, PIECE_WP);
        b.set_piece(A2, PIECE_WP);
        b.set_piece(C2, PIECE_WP);
        b.set_piece(D2, PIECE_WP);
        b.set_piece(C3, PIECE_BP);
        b.set_piece(F3, PIECE_BP);
        b.set_piece(G3, PIECE_BP);
        b.set_piece(H3, PIECE_BP);
        assert_eq!("8/8/8/8/8/2p2ppp/P1PP4/1P6", fen_board(&b));
    }

    #[test]
    fn test_fen_position() {
        let p = Position::from_initial();
        assert_eq!(
            "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0",
            fen_position(&p)
        );
    }

    #[test]
    fn test_parse_fen_board() {
        let b = parse_fen_board(FEN_BOARD_EMPTY).unwrap();
        assert!(b == Board::default());

        let b = parse_fen_board(FEN_BOARD_INITIAL).unwrap();
        assert!(b == Board::from_initial());

        let b = parse_fen_board("7r/6N1/5b2/4K3/3q4/2B5/1n6/R7").unwrap();
        assert_eq!(4, b.bitboard_by_color(COLOR_W).iter().count());
        assert!(b.get_piece(A1) == PIECE_WR);
        assert!(b.get_piece(C3) == PIECE_WB);
        assert!(b.get_piece(E5) == PIECE_WK);
        assert!(b.get_piece(G7) == PIECE_WN);
        assert_eq!(4, b.bitboard_by_color(COLOR_B).iter().count());
        assert!(b.get_piece(B2) == PIECE_BN);
        assert!(b.get_piece(D4) == PIECE_BQ);
        assert!(b.get_piece(F6) == PIECE_BB);
        assert!(b.get_piece(H8) == PIECE_BR);

        assert!(parse_fen_board("").is_err());
        assert!(parse_fen_board("8/8/8/8/8/8/8").is_err());
        assert!(parse_fen_board("8/8/8/8/8/8/8/8/8").is_err());
        assert!(parse_fen_board("0/8/8/8/8/8/8/8").is_err());
        assert!(parse_fen_board("9/8/8/8/8/8/8/8").is_err());
        assert!(parse_fen_board("7/8/8/8/8/8/8/8").is_err());
        assert!(parse_fen_board("8/P7P/8/8/8/8/8/8").is_err());
        assert!(parse_fen_board("8/P5P/8/8/8/8/8/8").is_err());
        assert!(parse_fen_board("8/a7/8/8/8/8/8/8").is_err());
    }

    #[test]
    fn test_parse_fen_position() {
        let p_str = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
        let p = parse_fen_position(p_str).unwrap();
        assert!(p.get_side_to_move() == COLOR_W);
        assert!(p.is_castling_able(Castling::WK));
        assert!(p.is_castling_able(Castling::WQ));
        assert!(p.is_castling_able(Castling::BK));
        assert!(p.is_castling_able(Castling::BQ));
        assert!(p.get_en_passant().is_none());
        assert!(p.get_half_move_clock() == 0);

        let p_str = "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1";
        let p = parse_fen_position(p_str).unwrap();
        assert!(p.get_side_to_move() == COLOR_B);
        assert!(p.is_castling_able(Castling::WK));
        assert!(p.is_castling_able(Castling::WQ));
        assert!(p.is_castling_able(Castling::BK));
        assert!(p.is_castling_able(Castling::BQ));
        assert!(p.get_en_passant().unwrap() == E3);
        assert!(p.get_half_move_clock() == 0);

        let p_str = "8/8/8/8/8/8/8/8 b KQ - 1";
        let p = parse_fen_position(p_str).unwrap();
        assert!(p.is_castling_able(Castling::WK));
        assert!(p.is_castling_able(Castling::WQ));
        assert!(!p.is_castling_able(Castling::BK));
        assert!(!p.is_castling_able(Castling::BQ));
        assert!(p.get_half_move_clock() == 1);
    }
}

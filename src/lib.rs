//     rchess - a Rust library to manage chess structures
//     Copyright (C) 2022-2024  Rafa Couto <caligari@treboada.net>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program. If not, see <https://www.gnu.org/licenses/>.

#[macro_use]
extern crate lazy_static;

pub mod board;
pub mod half_move;
pub mod notation;
pub mod piece;
pub mod position;
pub mod square;

pub mod bitboard;
mod magic_bitboards;
pub mod pseudo_legals;

pub mod fen;
mod zobrist;

pub mod prelude {
    pub use crate::bitboard::{Bitboard, BitboardTrait};
    pub use crate::board::Board;
    pub use crate::half_move::*;
    pub use crate::piece::*;
    pub use crate::position::*;
    pub use crate::square::*;
}

//     rchess - a Rust library to manage chess structures
//     Copyright (C) 2022-2024  Rafa Couto <caligari@treboada.net>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::notation::{parse_pure_coordinate, pure_coordinate};
use crate::prelude::*;

pub enum Flags {
    QuiteMove = 0b00000000,
    DoublePawnPush = 0b00000001,
    KingCastle = 0b00000010,
    QueenCastle = 0b00000011,
    EnPassant = 0b00000100,
    PromotionN = 0b00001000,
    PromotionB = 0b00001001,
    PromotionR = 0b00001010,
    PromotionQ = 0b00001011,
}

const FLAGS_MASK_PROMO_PIECE: u8 = 0b00000011;
const FLAGS_MASK_IS_PROMO: u8 = 0b00001000;

#[derive(Clone, Copy, PartialEq)]
pub struct HalfMove {
    origin: Square,
    target: Square,
    flags: u8,
}

impl HalfMove {
    pub fn new(origin: Square, target: Square) -> Self {
        HalfMove {
            origin,
            target,
            flags: Flags::QuiteMove as u8,
        }
    }

    #[inline]
    pub fn get_origin(&self) -> Square {
        self.origin
    }

    #[inline]
    pub fn get_target(&self) -> Square {
        self.target
    }

    pub fn promotion(origin: Square, target: Square, piece: PieceType) -> Self {
        HalfMove {
            origin,
            target,
            flags: FLAGS_MASK_IS_PROMO | (piece - PIECE_N),
        }
    }

    pub fn castling_k(color: Color) -> Self {
        let (origin, target) = match color {
            COLOR_W => (E1, G1),
            COLOR_B => (E8, G8),
            _ => unreachable!(),
        };
        HalfMove {
            origin,
            target,
            flags: Flags::KingCastle as u8,
        }
    }

    pub fn castling_q(color: Color) -> Self {
        let (origin, target) = match color {
            COLOR_W => (E1, C1),
            COLOR_B => (E8, C8),
            _ => unreachable!(),
        };
        HalfMove {
            origin,
            target,
            flags: Flags::QueenCastle as u8,
        }
    }

    pub fn en_passant_capture(origin: Square, target: Square) -> Self {
        HalfMove {
            origin,
            target,
            flags: Flags::EnPassant as u8,
        }
    }

    pub fn pawn_double_push(origin: Square, target: Square) -> Self {
        HalfMove {
            origin,
            target,
            flags: Flags::DoublePawnPush as u8,
        }
    }

    #[inline]
    pub fn is_promotion(&self) -> bool {
        self.flags & FLAGS_MASK_IS_PROMO != 0
    }

    #[inline]
    pub fn promotion_piece_type(&self) -> PieceType {
        (self.flags & FLAGS_MASK_PROMO_PIECE) + PIECE_N
    }

    #[inline]
    pub fn is_castling(&self) -> bool {
        self.flags == Flags::KingCastle as u8 || self.flags == Flags::QueenCastle as u8
    }

    #[inline]
    pub fn is_en_pasant(&self) -> bool {
        self.flags == Flags::EnPassant as u8
    }

    #[inline]
    pub fn is_pawn_double_push(&self) -> bool {
        self.flags == Flags::DoublePawnPush as u8
    }

    pub fn execute(&self, board: &mut Board) -> Piece {
        let moving_piece = board.get_piece(self.origin);
        debug_assert!(
            moving_piece.piece() != PIECE_0,
            "Bad move: no piece to move in origin square."
        );

        // movement
        let captured_piece = board.get_piece(self.target);
        board.remove_piece(self.origin);
        board.set_piece(self.target, moving_piece);

        // special moves
        if self.is_castling() {
            // move the rook
            let rook_move = self.castling_rook_move();
            board.set_piece(rook_move.target, board.get_piece(rook_move.origin));
            board.remove_piece(rook_move.origin);
        } else if self.is_en_pasant() {
            // remove the captured en-passant pawn
            let file = self.target.file();
            match board.get_color(self.target) {
                COLOR_W => board.remove_piece(Square::new(RANK_4, file)),
                COLOR_B => board.remove_piece(Square::new(RANK_5, file)),
                _ => unreachable!(),
            };
        } else if self.is_promotion() {
            // put promoted piece on target square
            let color = board.get_color(self.origin);
            let piece = self.promotion_piece_type();
            board.set_piece(self.target, Piece::new(piece, color));
        }

        captured_piece
    }

    pub fn castling_rook_move(&self) -> HalfMove {
        match self.target {
            G1 => HalfMove::new(H1, F1),
            G8 => HalfMove::new(H8, F8),
            C1 => HalfMove::new(A1, D1),
            C8 => HalfMove::new(A8, D8),
            _ => unreachable!(),
        }
    }

    pub fn parse(s: &str) -> Option<HalfMove> {
        parse_pure_coordinate(s)
    }
}

impl std::fmt::Debug for HalfMove {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", pure_coordinate(self))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_move_regular() {
        let mut board = Board::from_initial();
        HalfMove::new(D2, D4).execute(&mut board);
        assert!(!board.is_any_piece(D2));
        assert_eq!(PIECE_WP, board.get_piece(D4));
    }

    #[test]
    fn test_move_castling() {
        let mut board = Board::default();
        board.set_piece(E1, PIECE_WK);
        board.set_piece(H1, PIECE_WR);
        HalfMove::castling_k(COLOR_W).execute(&mut board);
        assert!(!board.is_any_piece(E1));
        assert!(!board.is_any_piece(H1));
        assert_eq!(PIECE_WK, board.get_piece(G1));
        assert_eq!(PIECE_WR, board.get_piece(F1));

        let mut board = Board::default();
        board.set_piece(E1, PIECE_WK);
        board.set_piece(A1, PIECE_WR);
        HalfMove::castling_q(COLOR_W).execute(&mut board);
        assert!(!board.is_any_piece(E1));
        assert!(!board.is_any_piece(A1));
        assert_eq!(PIECE_WK, board.get_piece(C1));
        assert_eq!(PIECE_WR, board.get_piece(D1));

        let mut board = Board::default();
        board.set_piece(E8, PIECE_BK);
        board.set_piece(H8, PIECE_BR);
        HalfMove::castling_k(COLOR_B).execute(&mut board);
        assert!(!board.is_any_piece(E8));
        assert!(!board.is_any_piece(H8));
        assert_eq!(PIECE_BK, board.get_piece(G8));
        assert_eq!(PIECE_BR, board.get_piece(F8));

        let mut board = Board::default();
        board.set_piece(E8, PIECE_BK);
        board.set_piece(A8, PIECE_BR);
        HalfMove::castling_q(COLOR_B).execute(&mut board);
        assert!(!board.is_any_piece(E8));
        assert!(!board.is_any_piece(A8));
        assert_eq!(PIECE_BK, board.get_piece(C8));
        assert_eq!(PIECE_BR, board.get_piece(D8));
    }
}

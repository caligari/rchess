//     rchess - a Rust library to manage chess structures
//     Copyright (C) 2022-2024  Rafa Couto <caligari@treboada.net>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::pseudo_legals::pseudo_legals;
use crate::fen::{fen_position, parse_fen_position, ParseFenError};
use crate::{prelude::*, zobrist};

pub enum Castling {
    None = 0,
    WK = 1 << 0,
    WQ = 1 << 1,
    BK = 1 << 2,
    BQ = 1 << 3,
}

const CASTLING_B: u8 = Castling::BK as u8 | Castling::BQ as u8;
const CASTLING_W: u8 = Castling::WK as u8 | Castling::WQ as u8;
const CASTLING_ALL: u8 = CASTLING_W | CASTLING_B;

pub struct ReversibleMove {
    half_move: HalfMove,
    captured: Piece,
    en_passant_target: Option<Square>,
    castling_ability: u8,
    half_move_clock: u8,
}

#[derive(Default, Clone, Copy, PartialEq)]
pub struct Position {
    board: Board,
    side_to_move: Color,
    castling_ability: u8,
    en_passant_target: Option<Square>,
    half_move_clock: u8,
}

impl Position {
    pub fn new() -> Self {
        Position {
            board: Board::default(),
            side_to_move: COLOR_W,
            castling_ability: Castling::None as u8,
            en_passant_target: None,
            half_move_clock: 0,
        }
    }

    pub fn from_initial() -> Self {
        Position {
            board: Board::from_initial(),
            side_to_move: COLOR_W,
            castling_ability: CASTLING_ALL,
            en_passant_target: None,
            half_move_clock: 0,
        }
    }

    pub fn with_params(
        board: &Board,
        side_to_move: Color,
        castling_ability: u8,
        en_passant_target: Option<Square>,
        half_move_clock: u8,
    ) -> Position {
        Position {
            board: *board,
            side_to_move,
            castling_ability,
            en_passant_target,
            half_move_clock,
        }
    }

    pub fn parse(fen: &str) -> Result<Self, ParseFenError> {
        parse_fen_position(fen)
    }

    #[inline]
    pub fn get_board(&self) -> &Board {
        &self.board
    }

    #[inline]
    pub fn get_mut_board(&mut self) -> &mut Board {
        &mut self.board
    }

    #[inline]
    pub fn get_side_to_move(&self) -> Color {
        self.side_to_move
    }

    #[inline]
    pub fn get_en_passant(&self) -> Option<Square> {
        self.en_passant_target
    }

    #[inline]
    pub fn get_half_move_clock(&self) -> u8 {
        self.half_move_clock
    }

    #[inline]
    pub fn is_castling_able(&self, c: Castling) -> bool {
        self.castling_ability & c as u8 != 0
    }

    #[inline]
    pub fn is_check(&self) -> bool {
        self.board.check_for_turn(self.side_to_move)
    }

    pub fn make_move(&mut self, m: &HalfMove) -> ReversibleMove {
        let piece = self.board.get_piece(m.get_origin());
        let capture = m.execute(&mut self.board);

        // info to unmake move
        let reversible = ReversibleMove {
            half_move: *m,
            captured: capture,
            en_passant_target: self.en_passant_target,
            castling_ability: self.castling_ability,
            half_move_clock: self.half_move_clock,
        };

        // en-passant square
        self.en_passant_target = if m.is_pawn_double_push() {
            let file = m.get_origin().file();
            let rank = if m.get_origin().rank() == RANK_2 {
                RANK_3
            } else {
                RANK_6
            };
            Some(Square::new(rank, file))
        } else {
            None
        };

        // 50-move rule
        if capture != PIECE_00 || piece.piece() == PIECE_P {
            self.half_move_clock = 0;
        } else {
            self.half_move_clock += 1;
        }

        // turn changed
        self.side_to_move *= -1;

        reversible
    }

    pub fn unmake_move(&mut self, rev: &ReversibleMove) {
        // restore piece
        let m = &rev.half_move;
        let mut piece = self.board.get_piece(m.get_target());
        if m.is_promotion() {
            piece = Piece::new(PIECE_P, piece.color());
        }
        self.board.set_piece(m.get_origin(), piece);

        // restore captured piece
        if rev.captured != PIECE_00 {
            let captured_square = if m.is_en_pasant() {
                let rank = [RANK_5, 0, RANK_4][(self.side_to_move + 1) as usize];
                let target = Square::new(rank, m.get_target().file());
                self.en_passant_target = Some(target);
                target
            } else {
                m.get_target()
            };
            self.board.set_piece(captured_square, rev.captured);
        } else {
            self.board.set_piece(m.get_target(), PIECE_00);
            if m.is_castling() {
                // restore rook
                let rook_move = m.castling_rook_move();
                let rook = self.board.get_piece(rook_move.get_target());
                self.board.set_piece(rook_move.get_origin(), rook);
                self.board.set_piece(rook_move.get_target(), PIECE_00);
            }
        }

        // restore position state
        self.en_passant_target = rev.en_passant_target;
        self.castling_ability = rev.castling_ability;
        self.half_move_clock = rev.half_move_clock;

        // turn changed
        self.side_to_move *= -1;
    }

    pub fn hash(&self) -> u64 {
        zobrist::hash_board(self.get_board())
            ^ zobrist::hash_side_to_move(self.side_to_move)
            ^ zobrist::hash_castling(self.castling_ability)
            ^ zobrist::hash_en_passant(self.en_passant_target)
    }

    pub fn get_legal_moves(&self) -> Vec<HalfMove> {
        let mut legals = Vec::new();
        self.add_pseudo_legals(&mut legals);

        // add castlings and en-passant captures
        match self.side_to_move {
            COLOR_W => {
                self.add_castlings_w(&mut legals);
                self.add_en_passants_w(&mut legals);
            }
            COLOR_B => {
                self.add_castlings_b(&mut legals);
                self.add_en_passants_b(&mut legals);
            }
            _ => unreachable!(),
        };

        // prune illegals by check
        legals
            .into_iter()
            .filter(|m| {
                let mut board = self.board;
                let piece = board.get_piece(m.get_origin());
                board.remove_piece(m.get_origin());
                board.set_piece(m.get_target(), piece);
                !board.check_for_turn(self.side_to_move)
            })
            .collect()
    }

    fn add_en_passants_w(&self, moves: &mut Vec<HalfMove>) {
        if let Some(target) = self.en_passant_target {
            self.board
                .en_passant_captures_w(target)
                .iter()
                .for_each(|origin| moves.push(HalfMove::en_passant_capture(origin, target)));
        }
    }

    fn add_en_passants_b(&self, moves: &mut Vec<HalfMove>) {
        if let Some(target) = self.en_passant_target {
            self.board
                .en_passant_captures_b(target)
                .iter()
                .for_each(|origin| moves.push(HalfMove::en_passant_capture(origin, target)));
        }
    }

    fn add_castlings_w(&self, moves: &mut Vec<HalfMove>) {
        if !self.is_check() && self.castling_ability & CASTLING_W != 0 {
            let (free_wk, free_wq) = self.board.castling_free_wk_wq();
            if free_wk && self.castling_ability & Castling::WK as u8 != 0 {
                moves.push(HalfMove::castling_k(COLOR_W));
            }
            if free_wq && self.castling_ability & Castling::WQ as u8 != 0 {
                moves.push(HalfMove::castling_q(COLOR_W));
            }
        }
    }

    fn add_castlings_b(&self, moves: &mut Vec<HalfMove>) {
        if !self.is_check() && self.castling_ability & CASTLING_B != 0 {
            let (free_bk, free_bq) = self.board.castling_free_bk_bq();
            if free_bk && self.castling_ability & Castling::BK as u8 != 0 {
                moves.push(HalfMove::castling_k(COLOR_B));
            }
            if free_bq && self.castling_ability & Castling::BQ as u8 != 0 {
                moves.push(HalfMove::castling_q(COLOR_B));
            }
        }
    }

    fn add_pseudo_legals(&self, moves: &mut Vec<HalfMove>) {
        self.board
            .bitboard_by_color(self.side_to_move)
            .iter()
            .for_each(|origin| {
                pseudo_legals(self.board, origin).iter().for_each(|target| {
                    if self.board.is_pawn(origin) && {
                        let r = target.rank();
                        r == RANK_1 || r == RANK_8
                    } {
                        moves.push(HalfMove::promotion(origin, target, PIECE_Q));
                        moves.push(HalfMove::promotion(origin, target, PIECE_R));
                        moves.push(HalfMove::promotion(origin, target, PIECE_B));
                        moves.push(HalfMove::promotion(origin, target, PIECE_N));
                    } else {
                        moves.push(HalfMove::new(origin, target));
                    }
                })
            });
    }
}

impl std::fmt::Debug for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Position(\"{}\")", fen_position(self))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_position_empty() {
        let p = Position::new();

        // empty board
        assert!(p.board == Board::default());

        // default turn is white
        assert!(p.side_to_move == COLOR_W);

        // no castlings
        assert!(!p.is_castling_able(Castling::WK));
        assert!(!p.is_castling_able(Castling::WQ));
        assert!(!p.is_castling_able(Castling::BK));
        assert!(!p.is_castling_able(Castling::BQ));

        // en passant target square
        assert!(p.en_passant_target.is_none());

        // half move clock respect to the 50 move draw rule
        assert!(p.half_move_clock == 0);
    }

    #[test]
    fn test_position_initial() {
        // position
        let p = Position::from_initial();

        // initial board
        assert!(p.board == Board::from_initial());

        // default turn is white
        assert!(p.side_to_move == COLOR_W);

        // white castlings are able
        assert!(p.is_castling_able(Castling::WK));
        assert!(p.is_castling_able(Castling::WQ));

        // black castlings are able
        assert!(p.is_castling_able(Castling::BK));
        assert!(p.is_castling_able(Castling::BQ));

        // en passant target square
        assert!(p.en_passant_target.is_none());

        // half move clock respect to the 50 move draw rule
        assert!(p.half_move_clock == 0);
    }

    #[test]
    fn test_execute_move() {
        let mut p = Position::from_initial();

        let mut m = HalfMove::pawn_double_push(E2, E4);
        let rev = p.make_move(&mut m);
        assert_eq!(PIECE_00, rev.captured);
        assert!(!p.board.is_any_piece(E2));
        assert!(p.board.get_piece(E4) == PIECE_WP);
        assert!(p.half_move_clock == 0);
        assert!(p.side_to_move == COLOR_B);
        assert!(p.en_passant_target == Some(E3));
        assert!(p.is_castling_able(Castling::WK));
        assert!(p.is_castling_able(Castling::WQ));

        let mut m = HalfMove::pawn_double_push(D7, D5);
        let rev = p.make_move(&mut m);
        assert_eq!(PIECE_00, rev.captured);
        assert!(!p.board.is_any_piece(D7));
        assert!(p.board.get_piece(D5) == PIECE_BP);
        assert!(p.half_move_clock == 0);
        assert!(p.side_to_move == COLOR_W);
        assert!(p.en_passant_target == Some(D6));
        assert!(p.is_castling_able(Castling::BK));
        assert!(p.is_castling_able(Castling::BQ));

        let mut m = HalfMove::new(E4, D5);
        let rev = p.make_move(&mut m);
        assert_eq!(PIECE_BP, rev.captured);
        assert!(!p.board.is_any_piece(E4));
        assert!(p.board.get_piece(D5) == PIECE_WP);
        assert!(p.half_move_clock == 0);
        assert!(p.side_to_move == COLOR_B);
        assert!(p.en_passant_target == None);
        assert!(p.is_castling_able(Castling::WK));
        assert!(p.is_castling_able(Castling::WQ));
    }

    #[test]
    fn test_pseudo_legals() {
        let mut pos = Position::from_initial();
        let legals = pos.get_legal_moves();
        let expected = [
            "a2a3", "a2a4", "b2b3", "b2b4", "c2c3", "c2c4", "d2d3", "d2d4", "e2e3", "e2e4", "f2f3",
            "f2f4", "g2g3", "g2g4", "h2h3", "h2h4", "b1a3", "b1c3", "g1f3", "g1h3",
        ];
        assert_eq!(expected.len(), legals.len());
        expected
            .iter()
            .for_each(|m| assert!(legals.contains(&HalfMove::parse(m).unwrap())));

        pos.make_move(&mut HalfMove::parse("e2e4").unwrap());
        let legals = pos.get_legal_moves();
        let expected = [
            "a7a6", "a7a5", "b7b6", "b7b5", "c7c6", "c7c5", "d7d6", "d7d5", "e7e6", "e7e5", "f7f6",
            "f7f5", "g7g6", "g7g5", "h7h6", "h7h5", "b8a6", "b8c6", "g8f6", "g8h6",
        ];
        assert_eq!(expected.len(), legals.len());
        expected
            .iter()
            .for_each(|m| assert!(legals.contains(&HalfMove::parse(m).unwrap())));

        pos.make_move(&mut HalfMove::parse("d7d5").unwrap());
        let legals = pos.get_legal_moves();
        let expected = [
            "b1a3", "b1c3", "d1e2", "d1f3", "d1g4", "d1h5", "e1e2", "f1e2", "f1d3", "f1c4", "f1b5",
            "f1a6", "g1e2", "g1f3", "g1h3", "a2a3", "a2a4", "b2b3", "b2b4", "c2c3", "c2c4", "d2d3",
            "d2d4", "f2f3", "f2f4", "g2g3", "g2g4", "h2h3", "h2h4", "e4d5", "e4e5",
        ];
        assert_eq!(expected.len(), legals.len());
        expected
            .iter()
            .for_each(|m| assert!(legals.contains(&HalfMove::parse(m).unwrap())));
    }

    #[test]
    fn test_castlings() {
        // white castlings
        let pos = Position::parse("r3k2r/8/8/8/8/8/8/R3K2R w KQkq - 0 1").unwrap();
        let legals = pos.get_legal_moves();
        assert!(legals.contains(&HalfMove::castling_k(COLOR_W)));
        assert!(legals.contains(&HalfMove::castling_q(COLOR_W)));

        // black castlings
        let pos = Position::parse("r3k2r/8/8/8/8/8/8/R3K2R b KQkq - 0 1").unwrap();
        let legals = pos.get_legal_moves();
        assert!(legals.contains(&HalfMove::castling_k(COLOR_B)));
        assert!(legals.contains(&HalfMove::castling_q(COLOR_B)));
    }

    #[test]
    fn test_invalid_castlings() {
        // check
        let p = Position::parse("4k3/4r3/8/8/8/8/8/R3K2R w KQ - 0 1").unwrap();
        [HalfMove::castling_k(COLOR_W), HalfMove::castling_q(COLOR_W)]
            .iter()
            .for_each(|m| assert!(!p.get_legal_moves().contains(&m)));
        let p = Position::parse("r3k2r/8/8/8/8/8/4R3/4K3 b kq - 0 1").unwrap();
        [HalfMove::castling_k(COLOR_B), HalfMove::castling_q(COLOR_B)]
            .iter()
            .for_each(|m| assert!(!p.get_legal_moves().contains(&m)));

        // attacked ways
        let p = Position::parse("4k3/3r1r2/8/8/8/8/8/R3K2R w KQ - 0 1").unwrap();
        [HalfMove::castling_k(COLOR_W), HalfMove::castling_q(COLOR_W)]
            .iter()
            .for_each(|m| assert!(!p.get_legal_moves().contains(&m)));
        let p = Position::parse("r3k2r/8/8/8/8/8/3R1R2/4K3 b kq - 0 1").unwrap();
        [HalfMove::castling_k(COLOR_B), HalfMove::castling_q(COLOR_B)]
            .iter()
            .for_each(|m| assert!(!p.get_legal_moves().contains(&m)));
    }

    #[test]
    fn test_en_passant_captures() {
        let p =
            Position::parse("rnbqkbnr/p1p1pppp/8/4P1P1/1pPp4/8/PP1P1P1P/RNBQKBNR b KQkq c3 0 1")
                .unwrap();
        let moves = p.get_legal_moves();
        assert!(moves.contains(&HalfMove::en_passant_capture(B4, C3)));
        assert!(moves.contains(&HalfMove::en_passant_capture(D4, C3)));
        let p =
            Position::parse("rnbqkbnr/p1p1p1pp/8/4PpP1/1pPp4/8/PP1P1P1P/RNBQKBNR w KQkq f6 0 1")
                .unwrap();
        let moves = p.get_legal_moves();
        assert!(moves.contains(&HalfMove::en_passant_capture(E5, F6)));
        assert!(moves.contains(&HalfMove::en_passant_capture(G5, F6)));
    }

    #[test]
    fn test_promotions() {
        // white promotions
        let p = Position::parse("r3k3/1P6/8/8/8/8/8/4K3 w - - 0 1").unwrap();
        let moves = p.get_legal_moves();
        assert!(moves.contains(&HalfMove::promotion(B7, B8, PIECE_Q)));
        assert!(moves.contains(&HalfMove::promotion(B7, B8, PIECE_R)));
        assert!(moves.contains(&HalfMove::promotion(B7, B8, PIECE_B)));
        assert!(moves.contains(&HalfMove::promotion(B7, B8, PIECE_N)));
        assert!(moves.contains(&HalfMove::promotion(B7, A8, PIECE_Q)));
        assert!(moves.contains(&HalfMove::promotion(B7, A8, PIECE_R)));
        assert!(moves.contains(&HalfMove::promotion(B7, A8, PIECE_B)));
        assert!(moves.contains(&HalfMove::promotion(B7, A8, PIECE_N)));
        // black promotions
        let p = Position::parse("4k3/8/8/8/8/8/6p1/4K2R b - - 0 1").unwrap();
        let moves = p.get_legal_moves();
        assert!(moves.contains(&HalfMove::promotion(G2, G1, PIECE_Q)));
        assert!(moves.contains(&HalfMove::promotion(G2, G1, PIECE_R)));
        assert!(moves.contains(&HalfMove::promotion(G2, G1, PIECE_B)));
        assert!(moves.contains(&HalfMove::promotion(G2, G1, PIECE_N)));
        assert!(moves.contains(&HalfMove::promotion(G2, H1, PIECE_Q)));
        assert!(moves.contains(&HalfMove::promotion(G2, H1, PIECE_R)));
        assert!(moves.contains(&HalfMove::promotion(G2, H1, PIECE_B)));
        assert!(moves.contains(&HalfMove::promotion(G2, H1, PIECE_N)));
    }

    #[test]
    fn test_pinned() {
        let p = Position::parse("4k3/4b3/8/8/8/8/8/4K3 b - - 0 1").unwrap();
        let moves = p.get_legal_moves();
        assert_eq!(13, moves.len());
        let p = Position::parse("4k3/4b3/8/8/8/8/4Q3/4K3 b - - 0 1").unwrap();
        let moves = p.get_legal_moves();
        assert_eq!(4, moves.len());
    }

    #[test]
    fn test_unmake_move() {
        let mut p = Position::from_initial();
        let original = p.clone();
        let m = HalfMove::pawn_double_push(E2, E4);
        let rev = p.make_move(&m);
        p.unmake_move(&rev);
        assert_eq!(original, p);

        p.make_move(&m);
        let original = p.clone();
        let m = HalfMove::pawn_double_push(D7, D5);
        let rev = p.make_move(&m);
        p.unmake_move(&rev);
        assert_eq!(original, p);

        p.make_move(&m);
        let original = p.clone();
        let m = HalfMove::new(E4, D5);
        let rev = p.make_move(&m);
        p.unmake_move(&rev);
        assert_eq!(original, p);
    }
}

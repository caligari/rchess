//     rchess - a Rust library to manage chess structures
//     Copyright (C) 2022-2024  Rafa Couto <caligari@treboada.net>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::prelude::*;

pub fn pure_coordinate(m: &HalfMove) -> String {
    if m.is_promotion() {
        format!(
            "{}{}{}",
            square_name(m.get_origin()),
            square_name(m.get_target()),
            piece_name(m.promotion_piece_type())
        )
    } else {
        format!(
            "{}{}",
            square_name(m.get_origin()),
            square_name(m.get_target())
        )
    }
}

fn valid_coordinate(coordinate_str: &str, from: usize) -> Option<u8> {
    let str = coordinate_str
        .chars()
        .skip(from)
        .take(2)
        .collect::<String>();
    parse_square(&str)
}

pub fn parse_pure_coordinate(s: &str) -> Option<HalfMove> {
    let origin = valid_coordinate(s, 0)?;
    let target = valid_coordinate(s, 2)?;
    match s.len() {
        4 => Some(HalfMove::new(origin, target)),
        5 => {
            let promotion = s.chars().nth(4).and_then(parse_piece)?;
            Some(HalfMove::promotion(origin, target, promotion.piece()))
        }
        _ => None,
    }
}

#[cfg(test)]
mod tests {
    use super::super::piece::*;
    use super::super::square::*;
    use super::*;

    #[test]
    fn test_pure_coordinate_notation() {
        // regular
        let pc = pure_coordinate(&HalfMove::new(E2, E4));
        assert!("e2e4" == pc);

        // castling
        let pc = pure_coordinate(&HalfMove::new(E8, G8));
        assert!("e8g8" == pc);

        // promotion
        let pc = pure_coordinate(&HalfMove::promotion(E7, G8, PIECE_Q));
        assert!("e7g8Q" == pc);
    }

    #[test]
    fn test_parse_pure_coordinate_simple() {
        let m_opt = parse_pure_coordinate("e2e4");
        assert!(m_opt.is_some());

        let m = m_opt.unwrap();
        assert!(m.get_origin() == E2);
        assert!(m.get_target() == E4);
        assert!(!m.is_promotion());

        // no valid notations
        assert!(parse_pure_coordinate("").is_none());
        assert!(parse_pure_coordinate("e2e").is_none());
        assert!(parse_pure_coordinate("e2ee").is_none());
        assert!(parse_pure_coordinate("e2e44").is_none());
        assert!(parse_pure_coordinate("e2e4a").is_none());
        assert!(parse_pure_coordinate("e2e4q0").is_none());
        assert!(parse_pure_coordinate("0-0").is_none());
        assert!(parse_pure_coordinate("O-O").is_none());
    }

    #[test]
    fn test_parse_pure_coordinate_promotion() {
        let m_opt = parse_pure_coordinate("d7c8q");
        assert!(m_opt.is_some());

        let m = m_opt.unwrap();
        assert!(m.get_origin() == D7);
        assert!(m.get_target() == C8);
        assert!(m.is_promotion());
        assert!(m.promotion_piece_type() == PIECE_Q);
    }
}

//     rchess - a Rust library to manage chess structures
//     Copyright (C) 2022-2024  Rafa Couto <caligari@treboada.net>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::square::*;

pub type Bitboard = u64;

pub const BITBOARD_EMPTY: Bitboard = 0;

pub const BITBOARD_INITIAL_QUEENS: Bitboard = //
    1 << D1    // white queen on d1
    | 1 << D8; // black queen on d8

pub const BITBOARD_INITIAL_KINGS: Bitboard = //
    1 << E1    // white king on e1
    | 1 << E8; //  black king on e8

pub const BITBOARD_INITIAL_KNIGHTS: Bitboard = //
    1 << B1    // white knight on b1
    | 1 << G1  // white knight on g1
    | 1 << B8  // black knight on b8
    | 1 << G8; //  black knight on g8

pub const BITBOARD_INITIAL_BISHOPS: Bitboard = //
    1 << C1    // white bishop on c1
    | 1 << F1  // white bishop on f1
    | 1 << C8  // black bishop on c8
    | 1 << F8; //  black bishop on f8

pub const BITBOARD_INITIAL_ROOKS: Bitboard = //
    1 << A1    // white rook on a1
    | 1 << H1  // white rook on h1
    | 1 << A8  // black rook on a8
    | 1 << H8; // black rook on h8

pub const BITBOARD_INITIAL_PAWNS: Bitboard = //
    1 << A2    // white pawn on a2
    | 1 << B2  // white pawn on b2
    | 1 << C2  // white pawn on c2
    | 1 << D2  // white pawn on d2
    | 1 << E2  // white pawn on e2
    | 1 << F2  // white pawn on f2
    | 1 << G2  // white pawn on g2
    | 1 << H2  // white pawn on h2
    | 1 << A7  // black pawn on a7
    | 1 << B7  // black pawn on b7
    | 1 << C7  // black pawn on c7
    | 1 << D7  // black pawn on d7
    | 1 << E7  // black pawn on e7
    | 1 << F7  // black pawn on f7
    | 1 << G7  // black pawn on g7
    | 1 << H7; // black pawn on h7

pub const BITBOARD_INITIAL_WHITES: Bitboard = //
    1 << A1 | 1 << B1 | 1 << C1 | 1 << D1 | 1 << E1 | 1 << F1 | 1 << G1 | 1 << H1 | // rank 1
    1 << A2 | 1 << B2 | 1 << C2 | 1 << D2 | 1 << E2 | 1 << F2 | 1 << G2 | 1 << H2; //  rank 2

pub const BITBOARD_INITIAL_BLACKS: Bitboard = //
    1 << A7 | 1 << B7 | 1 << C7 | 1 << D7 | 1 << E7 | 1 << F7 | 1 << G7 | 1 << H7 | // rank 7
    1 << A8 | 1 << B8 | 1 << C8 | 1 << D8 | 1 << E8 | 1 << F8 | 1 << G8 | 1 << H8; //  rank 8

pub const BITBOARD_EDGE_SQUARES: Bitboard = //
    1 << A1 | 1 << A2 | 1 << A3 | 1 << A4 | 1 << A5 | 1 << A6 | 1 << A7 | // west
    1 << A8 | 1 << B8 | 1 << C8 | 1 << D8 | 1 << E8 | 1 << F8 | 1 << G8 | // north
    1 << H8 | 1 << H7 | 1 << H6 | 1 << H5 | 1 << H4 | 1 << H3 | 1 << H2 | // east
    1 << H1 | 1 << G1 | 1 << F1 | 1 << E1 | 1 << D1 | 1 << C1 | 1 << B1; //  south

pub const BITBOARD_CASTLING_WK_CHECK: Bitboard = 1 << E1 | 1 << F1 | 1 << G1;
pub const BITBOARD_CASTLING_WQ_CHECK: Bitboard = 1 << E1 | 1 << D1 | 1 << C1;
pub const BITBOARD_CASTLING_BK_CHECK: Bitboard = 1 << E8 | 1 << F8 | 1 << G8;
pub const BITBOARD_CASTLING_BQ_CHECK: Bitboard = 1 << E8 | 1 << D8 | 1 << C8;

pub const BITBOARD_CASTLING_WK_PATH: Bitboard = 1 << F1 | 1 << G1;
pub const BITBOARD_CASTLING_WQ_PATH: Bitboard = 1 << D1 | 1 << C1 | 1 << B1;
pub const BITBOARD_CASTLING_BK_PATH: Bitboard = 1 << F8 | 1 << G8;
pub const BITBOARD_CASTLING_BQ_PATH: Bitboard = 1 << D8 | 1 << C8 | 1 << B8;

pub trait BitboardTrait {
    fn is_set(&self, square: u8) -> bool;
    fn set(&mut self, square: u8);
    fn clear(&mut self, square: u8);
    fn is_empty(&self) -> bool;
    fn active_bits_count(&self) -> u8;
    fn flip_vertical(&self) -> Self;
    fn bit_scan_forward(&self) -> Square;
    fn bit_scan_reverse(&self) -> Square;
    fn lsb(&self) -> Option<Square>;
    fn msb(&self) -> Option<Square>;
    fn rank_bits(&self, rank: Rank) -> u8;
    fn file_bits(&self, file: File) -> u8;
    fn lines(&self) -> String;
    fn iter(&self) -> BitboardIter;
}

impl BitboardTrait for u64 {
    #[inline]
    fn is_set(&self, square: u8) -> bool {
        (1 << square) & self != 0
    }

    #[inline]
    fn set(&mut self, square: u8) {
        *self |= 1 << square;
    }

    #[inline]
    fn clear(&mut self, square: u8) {
        *self &= !(1 << square);
    }

    #[inline]
    fn is_empty(&self) -> bool {
        *self == 0
    }

    #[inline]
    fn active_bits_count(&self) -> u8 {
        self.count_ones() as u8
    }

    #[inline]
    fn flip_vertical(&self) -> Self {
        self.swap_bytes()
    }

    #[inline]
    fn bit_scan_forward(&self) -> Square {
        self.trailing_zeros() as Square
    }

    #[inline]
    fn bit_scan_reverse(&self) -> Square {
        (63 - self.leading_zeros()) as Square
    }

    fn lsb(&self) -> Option<Square> {
        (0..64_u8).find(|pos| self & (1 << pos) != 0)
    }

    fn msb(&self) -> Option<Square> {
        (0..64_u8).rev().find(|pos| self & (1 << pos) != 0)
    }

    fn rank_bits(&self, rank: Rank) -> u8 {
        (self >> (rank * 8) & 0xff_u64) as u8
    }

    fn file_bits(&self, file: File) -> u8 {
        let tmp = self >> (file % 8);
        ((tmp & 0b00000001)
            | ((tmp >> 7) & 0b00000010)
            | ((tmp >> 14) & 0b00000100)
            | ((tmp >> 21) & 0b00001000)
            | ((tmp >> 28) & 0b00010000)
            | ((tmp >> 35) & 0b00100000)
            | ((tmp >> 42) & 0b01000000)
            | ((tmp >> 49) & 0b10000000)) as u8
    }

    /// Builds a 8-line string with the bitboard representation
    fn lines(&self) -> String {
        (RANK_1..=RANK_8)
            .rev()
            .map(|r| {
                (FILE_A..=FILE_H)
                    .map(|f| match self.is_set(Square::new(r, f)) {
                        true => "1",
                        false => ".",
                    })
                    .collect::<Vec<&str>>()
                    .join(" ")
            })
            .collect::<Vec<String>>()
            .join("\n")
    }

    fn iter(&self) -> BitboardIter {
        BitboardIter::new(*self)
    }
}

pub struct BitboardIter {
    bitboard: Bitboard,
    current: Square,
}

impl BitboardIter {
    pub fn new(bitboard: Bitboard) -> Self {
        BitboardIter {
            bitboard,
            current: bitboard.bit_scan_forward(),
        }
    }
}

impl Iterator for BitboardIter {
    type Item = Square;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current > H8 {
            None
        } else {
            let n = self.current;
            self.bitboard.clear(n);
            self.current = self.bitboard.bit_scan_forward();
            Some(n)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bitboard() {
        let mut bb = BITBOARD_EMPTY;

        assert!(bb.is_empty());
        assert!(!bb.is_set(A1));

        // set square
        bb.set(A1);
        assert!(!bb.is_empty());
        assert!(bb.is_set(A1));
        assert!(!bb.is_set(A2));

        // clear square
        bb.clear(A1);
        assert!(!bb.is_set(A1));
        assert!(bb.is_empty());
    }

    #[test]
    fn test_bit_scan_forward() {
        let mut bb = BITBOARD_EMPTY;
        bb.set(D4);
        bb.set(D5);
        assert_eq!(D4, bb.bit_scan_forward());
        bb.set(C4);
        assert_eq!(C4, bb.bit_scan_forward());
        bb.set(D3);
        assert_eq!(D3, bb.bit_scan_forward());
    }

    #[test]
    fn test_bit_scan_reverse() {
        let mut bb = BITBOARD_EMPTY;
        bb.set(D4);
        bb.set(D5);
        assert_eq!(D5, bb.bit_scan_reverse());
        bb.set(E5);
        assert_eq!(E5, bb.bit_scan_reverse());
        bb.set(F6);
        assert_eq!(F6, bb.bit_scan_reverse());
    }

    #[test]
    fn test_file_bits() {
        let mut bb = BITBOARD_INITIAL_WHITES;
        assert!(0b00000011 == bb.file_bits(FILE_D));

        bb.set(D8);
        assert!(0b10000011 == bb.file_bits(FILE_D));

        bb.clear(D2);
        assert!(0b10000001 == bb.file_bits(FILE_D));
    }

    #[test]
    fn test_rank_bits() {
        let mut bb = BITBOARD_INITIAL_WHITES;
        assert!(0b11111111 == bb.rank_bits(RANK_1));

        bb.clear(A1);
        assert!(0b11111110 == bb.rank_bits(RANK_1));
    }

    #[test]
    fn test_lines() {
        let board = "\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .";
        assert_eq!(board, BITBOARD_EMPTY.lines());

        let board = "\
             . . . . . . . .\n\
             1 1 1 1 1 1 1 1\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             1 1 1 1 1 1 1 1\n\
             . . . . . . . .";
        assert_eq!(board, BITBOARD_INITIAL_PAWNS.lines());

        let board = "\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             . . . . . . . .\n\
             1 1 1 1 1 1 1 1\n\
             1 1 1 1 1 1 1 1";
        assert_eq!(board, BITBOARD_INITIAL_WHITES.lines());
    }

    #[test]
    fn test_bitboard_iter() {
        let mut iter = BITBOARD_INITIAL_WHITES.iter();
        assert_eq!(Some(A1), iter.next());
        assert_eq!(Some(B1), iter.next());
        assert_eq!(Some(C1), iter.next());
        assert_eq!(Some(D1), iter.next());
        assert_eq!(Some(E1), iter.next());
        assert_eq!(Some(F1), iter.next());
        assert_eq!(Some(G1), iter.next());
        assert_eq!(Some(H1), iter.next());
        assert_eq!(Some(A2), iter.next());
        assert_eq!(Some(B2), iter.next());
        assert_eq!(Some(C2), iter.next());
        assert_eq!(Some(D2), iter.next());
        assert_eq!(Some(E2), iter.next());
        assert_eq!(Some(F2), iter.next());
        assert_eq!(Some(G2), iter.next());
        assert_eq!(Some(H2), iter.next());
        assert_eq!(None, iter.next());

        let mut iter = BITBOARD_INITIAL_KNIGHTS.iter();
        assert_eq!(Some(B1), iter.next());
        assert_eq!(Some(G1), iter.next());
        assert_eq!(Some(B8), iter.next());
        assert_eq!(Some(G8), iter.next());
        assert_eq!(None, iter.next());
    }
}

//     rchess - a Rust library to manage chess structures
//     Copyright (C) 2022-2024  Rafa Couto <caligari@treboada.net>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published
//     by the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//     GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
//     along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::bitboard::*;
use super::square::*;

/// Returns the line-attacks of rook using _magic bitboards_ (perfect hashing).
pub fn get_attacks_r(occupancy: Bitboard, square: Square) -> Bitboard {
    let s = square as usize;
    let mut magic_index = occupancy & ROOK_MASKS[s];
    magic_index = magic_index.wrapping_mul(ROOK_MAGICS[s]) >> ROOK_SHIFT[s];
    ROOK_MAGIC_ATTACKS[s][magic_index as usize]
}

/// Returns the line-attacks of bishop using _magic bitboards_ (perfect hashing).
pub fn get_attacks_b(occupancy: Bitboard, square: Square) -> Bitboard {
    let s = square as usize;
    let mut magic_index = occupancy & BISHOP_MASKS[s];
    magic_index = magic_index.wrapping_mul(BISHOP_MAGICS[s]) >> BISHOP_SHIFT[s];
    BISHOP_MAGIC_ATTACKS[s][magic_index as usize]
}

const BLOCKERS_SIZE_B: usize = 1 << 10;
const BLOCKERS_SIZE_R: usize = 1 << 12;

type Ray = usize;
const RAY_N: Ray = 0;
const RAY_S: Ray = 1;
const RAY_E: Ray = 2;
const RAY_W: Ray = 3;
const RAY_NE: Ray = 4;
const RAY_NW: Ray = 5;
const RAY_SE: Ray = 6;
const RAY_SW: Ray = 7;

lazy_static! {
    // RAYS[square][ray]
    static ref RAYS: Vec<Vec<u64>> = {
        BOARD_SQUARES.iter().map(|&s| {
            (RAY_N..=RAY_SW).map(|r| get_ray(r, s)).collect()
        }).collect()
    };

    // ROOK_MASKS[square]
    static ref ROOK_MASKS: Vec<u64> = {
        BOARD_SQUARES.iter().map(|&s| {
            rook_mask(s)
        }).collect()
    };

    // BISHOP_MASKS[square]
    static ref BISHOP_MASKS: Vec<u64> = {
        BOARD_SQUARES.iter().map(|&s| {
            bishop_mask(s)
        }).collect()
    };

    // ROOK_MAGIC_ATTACKS[square][index]
    static ref ROOK_MAGIC_ATTACKS: Vec<Vec<Bitboard>> = {
        BOARD_SQUARES.iter().map(|&s| {
            let mut attacks = vec![Bitboard::default(); BLOCKERS_SIZE_R];
            (0..1 << (64-ROOK_SHIFT[s as usize])).for_each(|index| {
                let blockers = get_blockers_from_index(index, ROOK_MASKS[s as usize]);
                let magic_index = blockers.wrapping_mul(ROOK_MAGICS[s as usize]) >> ROOK_SHIFT[s as usize];
                attacks[magic_index as usize] = precalc_attacks_r(s, blockers);
            });
            attacks
        }).collect()
    };

    // BISHOP_MAGIC_ATTACKS[square][index]
    static ref BISHOP_MAGIC_ATTACKS: Vec<Vec<Bitboard>> = {
        BOARD_SQUARES.iter().map(|&s| {
            let mut attacks = vec![Bitboard::default(); BLOCKERS_SIZE_B];
            (0..1 << (64-BISHOP_SHIFT[s as usize])).for_each(|index| {
                let blockers = get_blockers_from_index(index, BISHOP_MASKS[s as usize]);
                let magic_index = blockers.wrapping_mul(BISHOP_MAGICS[s as usize]) >> BISHOP_SHIFT[s as usize];
                attacks[magic_index as usize] = precalc_attacks_b(s, blockers);
            });
            attacks
        }).collect()
    };
}

fn get_blockers_from_index(index: u64, mask: u64) -> u64 {
    let mut blockers = 0_u64;
    let mut mask = mask;
    (0..mask.count_ones()).for_each(|i| {
        let bit_pos = mask.trailing_zeros();
        mask &= !(1 << bit_pos);
        if index & (1 << i) != 0 {
            blockers |= 1 << bit_pos;
        }
    });
    blockers
}

fn rook_mask(origin: Square) -> u64 {
    (RAYS[origin as usize][RAY_N] & !0xff00000000000000)
        | (RAYS[origin as usize][RAY_S] & !0x00000000000000ff)
        | (RAYS[origin as usize][RAY_E] & !0x8080808080808080)
        | (RAYS[origin as usize][RAY_W] & !0x0101010101010101)
}

fn bishop_mask(origin: Square) -> u64 {
    (RAYS[origin as usize][RAY_NE]
        | RAYS[origin as usize][RAY_SE]
        | RAYS[origin as usize][RAY_SW]
        | RAYS[origin as usize][RAY_NW])
        & !BITBOARD_EDGE_SQUARES
}

fn get_ray(r: Ray, s: Square) -> u64 {
    match r {
        RAY_N => ray_with_deltas(s, 0, 1),
        RAY_S => ray_with_deltas(s, 0, -1),
        RAY_E => ray_with_deltas(s, 1, 0),
        RAY_W => ray_with_deltas(s, -1, 0),
        RAY_NW => ray_with_deltas(s, 1, 1),
        RAY_NE => ray_with_deltas(s, -1, 1),
        RAY_SW => ray_with_deltas(s, 1, -1),
        RAY_SE => ray_with_deltas(s, -1, -1),
        _ => unreachable!(),
    }
}

fn ray_with_deltas(square: Square, delta_file: isize, delta_rank: isize) -> u64 {
    let mut bb = Bitboard::default();
    let mut next = next_ray_square(square, delta_file, delta_rank);
    while next.is_some() {
        let s = next.unwrap();
        bb.set(s);
        next = next_ray_square(s, delta_file, delta_rank);
    }
    bb
}

fn next_ray_square(square: Square, delta_file: isize, delta_rank: isize) -> Option<Square> {
    let file = square.file() as isize + delta_file;
    let rank = square.rank() as isize + delta_rank;
    if (0..8).contains(&file) && (0..8).contains(&rank) {
        Some(Square::new(rank as Rank, file as File))
    } else {
        None
    }
}

fn precalc_attacks_r(s: Square, blockers: u64) -> Bitboard {
    let mut attacks = 0_u64;

    let ray = RAYS[s as usize][RAY_N];
    attacks |= ray;
    let ray_blockers = ray & blockers;
    if ray_blockers != 0 {
        attacks &= !(RAYS[bit_scan_forward(ray_blockers)][RAY_N]);
    }

    let ray = RAYS[s as usize][RAY_S];
    attacks |= ray;
    let ray_blockers = ray & blockers;
    if ray_blockers != 0 {
        attacks &= !(RAYS[bit_scan_reverse(ray_blockers)][RAY_S]);
    }

    let ray = RAYS[s as usize][RAY_E];
    attacks |= ray;
    let ray_blockers = ray & blockers;
    if ray_blockers != 0 {
        attacks &= !(RAYS[bit_scan_forward(ray_blockers)][RAY_E]);
    }

    let ray = RAYS[s as usize][RAY_W];
    attacks |= ray;
    let ray_blockers = ray & blockers;
    if ray_blockers != 0 {
        attacks &= !(RAYS[bit_scan_reverse(ray_blockers)][RAY_W]);
    }

    attacks
}

fn precalc_attacks_b(s: Square, blockers: u64) -> Bitboard {
    let mut attacks = 0_u64;

    let ray = RAYS[s as usize][RAY_NW];
    attacks |= ray;
    let ray_blockers = ray & blockers;
    if ray_blockers != 0 {
        attacks &= !(RAYS[bit_scan_forward(ray_blockers)][RAY_NW]);
    }

    let ray = RAYS[s as usize][RAY_NE];
    attacks |= ray;
    let ray_blockers = ray & blockers;
    if ray_blockers != 0 {
        attacks &= !(RAYS[bit_scan_forward(ray_blockers)][RAY_NE]);
    }

    let ray = RAYS[s as usize][RAY_SE];
    attacks |= ray;
    let ray_blockers = ray & blockers;
    if ray_blockers != 0 {
        attacks &= !(RAYS[bit_scan_reverse(ray_blockers)][RAY_SE]);
    }

    let ray = RAYS[s as usize][RAY_SW];
    attacks |= ray;
    let ray_blockers = ray & blockers;
    if ray_blockers != 0 {
        attacks &= !(RAYS[bit_scan_reverse(ray_blockers)][RAY_SW]);
    }

    attacks
}

#[inline]
fn bit_scan_forward(bb: u64) -> usize {
    debug_assert!(bb != 0);
    bb.trailing_zeros() as usize
}

#[inline]
fn bit_scan_reverse(bb: u64) -> usize {
    debug_assert!(bb != 0);
    (63 - bb.leading_zeros()) as usize
}

const ROOK_SHIFT: [u64; 64] = [
    64 - 12,
    64 - 11,
    64 - 11,
    64 - 11,
    64 - 11,
    64 - 11,
    64 - 11,
    64 - 12,
    64 - 11,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 11,
    64 - 11,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 11,
    64 - 11,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 11,
    64 - 11,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 11,
    64 - 11,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 11,
    64 - 11,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 10,
    64 - 11,
    64 - 12,
    64 - 11,
    64 - 11,
    64 - 11,
    64 - 11,
    64 - 11,
    64 - 11,
    64 - 12,
];

const BISHOP_SHIFT: [u64; 64] = [
    64 - 6,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 6,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 7,
    64 - 7,
    64 - 7,
    64 - 7,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 7,
    64 - 9,
    64 - 9,
    64 - 7,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 7,
    64 - 9,
    64 - 9,
    64 - 7,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 7,
    64 - 7,
    64 - 7,
    64 - 7,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 6,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 5,
    64 - 6,
];

const ROOK_MAGICS: [u64; 64] = [
    0x0a8002c000108020,
    0x06c00049b0002001,
    0x0100200010090040,
    0x2480041000800801,
    0x0280028004000800,
    0x0900410008040022,
    0x0280020001001080,
    0x2880002041000080,
    0xa000800080400034,
    0x0004808020004000,
    0x2290802004801000,
    0x0411000d00100020,
    0x0402800800040080,
    0x000b000401004208,
    0x2409000100040200,
    0x0001002100004082,
    0x0022878001e24000,
    0x1090810021004010,
    0x0801030040200012,
    0x0500808008001000,
    0x0a08018014000880,
    0x8000808004000200,
    0x0201008080010200,
    0x0801020000441091,
    0x0000800080204005,
    0x1040200040100048,
    0x0000120200402082,
    0x0d14880480100080,
    0x0012040280080080,
    0x0100040080020080,
    0x9020010080800200,
    0x0813241200148449,
    0x0491604001800080,
    0x0100401000402001,
    0x4820010021001040,
    0x0400402202000812,
    0x0209009005000802,
    0x0810800601800400,
    0x4301083214000150,
    0x204026458e001401,
    0x0040204000808000,
    0x8001008040010020,
    0x8410820820420010,
    0x1003001000090020,
    0x0804040008008080,
    0x0012000810020004,
    0x1000100200040208,
    0x430000a044020001,
    0x0280009023410300,
    0x00e0100040002240,
    0x0000200100401700,
    0x2244100408008080,
    0x0008000400801980,
    0x0002000810040200,
    0x8010100228810400,
    0x2000009044210200,
    0x4080008040102101,
    0x0040002080411d01,
    0x2005524060000901,
    0x0502001008400422,
    0x489a000810200402,
    0x0001004400080a13,
    0x4000011008020084,
    0x0026002114058042,
];

const BISHOP_MAGICS: [u64; 64] = [
    0x89a1121896040240,
    0x2004844802002010,
    0x2068080051921000,
    0x62880a0220200808,
    0x0004042004000000,
    0x0100822020200011,
    0xc00444222012000a,
    0x0028808801216001,
    0x0400492088408100,
    0x0201c401040c0084,
    0x00840800910a0010,
    0x0000082080240060,
    0x2000840504006000,
    0x30010c4108405004,
    0x1008005410080802,
    0x8144042209100900,
    0x0208081020014400,
    0x004800201208ca00,
    0x0f18140408012008,
    0x1004002802102001,
    0x0841000820080811,
    0x0040200200a42008,
    0x0000800054042000,
    0x88010400410c9000,
    0x0520040470104290,
    0x1004040051500081,
    0x2002081833080021,
    0x000400c00c010142,
    0x941408200c002000,
    0x0658810000806011,
    0x0188071040440a00,
    0x4800404002011c00,
    0x0104442040404200,
    0x0511080202091021,
    0x0004022401120400,
    0x80c0040400080120,
    0x8040010040820802,
    0x0480810700020090,
    0x0102008e00040242,
    0x0809005202050100,
    0x8002024220104080,
    0x0431008804142000,
    0x0019001802081400,
    0x0200014208040080,
    0x3308082008200100,
    0x041010500040c020,
    0x4012020c04210308,
    0x208220a202004080,
    0x0111040120082000,
    0x6803040141280a00,
    0x2101004202410000,
    0x8200000041108022,
    0x0000021082088000,
    0x0002410204010040,
    0x0040100400809000,
    0x0822088220820214,
    0x0040808090012004,
    0x00910224040218c9,
    0x0402814422015008,
    0x0090014004842410,
    0x0001000042304105,
    0x0010008830412a00,
    0x2520081090008908,
    0x40102000a0a60140,
];

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_rays_r() {
        let s = D4;
        let bb: Bitboard =
            (get_ray(RAY_N, s) | get_ray(RAY_S, s) | get_ray(RAY_E, s) | get_ray(RAY_W, s)).into();
        assert_eq!(
            bb.lines(),
            ". . . 1 . . . .\n\
             . . . 1 . . . .\n\
             . . . 1 . . . .\n\
             . . . 1 . . . .\n\
             1 1 1 . 1 1 1 1\n\
             . . . 1 . . . .\n\
             . . . 1 . . . .\n\
             . . . 1 . . . ."
        );

        let s = A1;
        let bb: Bitboard =
            (get_ray(RAY_N, s) | get_ray(RAY_S, s) | get_ray(RAY_E, s) | get_ray(RAY_W, s)).into();
        assert_eq!(
            bb.lines(),
            "1 . . . . . . .\n\
             1 . . . . . . .\n\
             1 . . . . . . .\n\
             1 . . . . . . .\n\
             1 . . . . . . .\n\
             1 . . . . . . .\n\
             1 . . . . . . .\n\
             . 1 1 1 1 1 1 1"
        );
    }
}

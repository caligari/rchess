# rchess

An experimental chess engine coded in Rust language.

[![pipeline status](https://gitlab.com/caligari/rchess/badges/master/pipeline.svg)](https://gitlab.com/caligari/rchess/-/commits/master)
[![FOSSA Status](https://app.fossa.com/api/projects/custom%2B43229%2Frchess.svg?type=small)](https://app.fossa.com/projects/custom%2B43229%2Frchess?ref=badge_small)

## Plan

- [x] Board representation
    - [x] Bitboards
    - [x] Half-move
- [x] Move generation
    - [x] Magic bitboards
    - [x] Pseudo-legal moves
    - [x] Chess rules
- [ ] Interface
    - [x] FEN
    - [ ] UCI
- [ ] Perft
    - [ ] Move tree generation
    - [x] Node hashing

## License

See [LICENSE](LICENSE) about licensing details.

## Acknowledgments

- Many thanks to every contributor to the great [Chess Programming Wiki][cpw01].
- Thanks to [GitLab][glb01] for hosting the repository and running the
  pipelines.
- Thanks to the [Rust community][rcy01] for creating so delicious programming
  language.
- Thanks to Rhys Rustad-Elliott (GunshipPenguin) for his [post about *magic
  bitboards*][shb01] that really helped to understand magic bitboards.

[cpw01]: https://www.chessprogramming.org
[glb01]: https://gitlab.com
[rcy01]: https://www.rust-lang.org/community
[shb01]: https://rhysre.net/fast-chess-move-generation-with-magic-bitboards.html
